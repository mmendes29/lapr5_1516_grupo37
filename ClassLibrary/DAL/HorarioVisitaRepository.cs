﻿using ClassLibrary.DTO;
using System.Collections.Generic;
using ClassLibrary.Models;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class HorarioVisitaRepository : IRepository<HorarioVisitaDTO>
    {
        public ImoveisDbContext context;

        public HorarioVisitaRepository()
        {
            context = new ImoveisDbContext();
        }

        public HorarioVisitaDTO Create(HorarioVisitaDTO horario)
        {
            HorarioVisita h = new HorarioVisita();
            h.HoraAbertura = horario.HoraAbertura;
            h.HoraFecho = horario.HoraFecho;
            h.DiasSemana = horario.DiasSemana;
            h.DuraçãoVisita = horario.DuraçãoVisita;

            context.HorariosVisita.Add(h);
            context.SaveChanges();
            return horario;
        }

        public bool Delete(int id)
        {
            var a = context.HorariosVisita.Find(id);
            if (a != null)
            {
                context.HorariosVisita.Remove(a);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<HorarioVisitaDTO> GetData()
        {
            List<HorarioVisita> horarios = context.HorariosVisita.ToList();
            List<HorarioVisitaDTO> dto = new List<HorarioVisitaDTO>();
            foreach (HorarioVisita h in horarios)
            {
                HorarioVisitaDTO tmp = new HorarioVisitaDTO(h);
                dto.Add(tmp);
            }
            return dto;
        }

        public HorarioVisitaDTO GetData(int id)
        {
            return new HorarioVisitaDTO(context.HorariosVisita.Find(id));
        }

        public bool Update(int id, HorarioVisitaDTO horario)
        {
            var h = context.HorariosVisita.Find(id);
            if (h != null)
            {
                h.HoraAbertura = horario.HoraAbertura;
                h.HoraFecho = horario.HoraFecho;
                h.DiasSemana = horario.DiasSemana;
                h.DuraçãoVisita = horario.DuraçãoVisita;
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
