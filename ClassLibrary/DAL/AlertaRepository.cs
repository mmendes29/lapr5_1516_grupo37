﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class AlertaRepository : IRepository<AlertaDTO>
    {
        public ImoveisDbContext context;

        public AlertaRepository()
        {
            context = new ImoveisDbContext();
        }

        public List<AlertaDTO> GetData()
        {
            List<Alerta> alertas = context.Alertas.ToList();
            List<AlertaDTO> dto = new List<AlertaDTO>();
            foreach (Alerta t in alertas)
            {
                AlertaDTO tmp = new AlertaDTO(t);
                dto.Add(tmp);

            }
            return dto;
        }

        public AlertaDTO GetData(int id)
        {
            return new AlertaDTO(context.Alertas.Find(id));
        }

        public AlertaDTO Create(AlertaDTO alerta)
        {
            Alerta a = new Alerta();
            a.fixo = new List<ParametroFixo>();
            a.parametroAlfaNumerico = new List<ParametroConstantesAlfaNumerico>();
            a.parametroIntervaloNumerico = new List<ParametroConstantesIntervaloNumerico>();
            
            foreach(List<string> l in alerta.fixo)
            {
                ParametroFixo p = new ParametroFixo();
                p.nome = l.ElementAt(1);
                p.valor_string = l.ElementAt(2);

                a.fixo.Add(p);

            }

            foreach (List<string> l in alerta.parametroAlfaNumerico)
            {
                ParametroConstantesAlfaNumerico p = new ParametroConstantesAlfaNumerico();
                p.nome = l.ElementAt(1);
                p.str = l.ElementAt(2);

                a.parametroAlfaNumerico.Add(p);

            }

            foreach (List<string> l in alerta.parametroIntervaloNumerico)
            {
                ParametroConstantesIntervaloNumerico p = new ParametroConstantesIntervaloNumerico();
                p.nome = l.ElementAt(1);
                p.min = float.Parse(l.ElementAt(2));
                p.max = float.Parse(l.ElementAt(3));

                a.parametroIntervaloNumerico.Add(p);

            }

            a.ApplicationUserID = alerta.applicationUserID;

            context.Alertas.Add(a);
            context.SaveChanges();
            return alerta;
        }

        public bool Update(int id, AlertaDTO alerta)
        {
            Alerta a = context.Alertas.Find(id);

            for (int i = 0; i < a.fixo.Count(); i++)
            {
                ParametroFixo p = context.ParametroFixoes.Find(a.fixo.ElementAt(i).ID);
                context.ParametroFixoes.Remove(p);
            }

            for (int i = 0; i < a.parametroAlfaNumerico.Count(); i++)
            {
                ParametroConstantesAlfaNumerico p = context.ParametroConstantesAlfaNumericoes.Find(a.parametroAlfaNumerico.ElementAt(i).ID);
                context.ParametroConstantesAlfaNumericoes.Remove(p);
            }

            for (int i = 0; i < a.parametroIntervaloNumerico.Count(); i++)
            {
                ParametroConstantesIntervaloNumerico p = context.ParametroConstantesIntervaloNumericoes.Find(a.parametroIntervaloNumerico.ElementAt(i).ID);
                context.ParametroConstantesIntervaloNumericoes.Remove(p);
            }

            a.fixo = new List<ParametroFixo>();
            a.parametroAlfaNumerico = new List<ParametroConstantesAlfaNumerico>();
            a.parametroIntervaloNumerico = new List<ParametroConstantesIntervaloNumerico>();


            foreach (List<string> l in alerta.fixo)
            {
                ParametroFixo p = new ParametroFixo();
                p.nome = l.ElementAt(1);
                p.valor_string = l.ElementAt(2);

                a.fixo.Add(p);

            }

            foreach (List<string> l in alerta.parametroAlfaNumerico)
            {
                ParametroConstantesAlfaNumerico p = new ParametroConstantesAlfaNumerico();
                p.nome = l.ElementAt(1);
                p.str = l.ElementAt(2);

                a.parametroAlfaNumerico.Add(p);

            }

            foreach (List<string> l in alerta.parametroIntervaloNumerico)
            {
                ParametroConstantesIntervaloNumerico p = new ParametroConstantesIntervaloNumerico();
                p.nome = l.ElementAt(1);
                if (l.ElementAt(2)=="")
                {
                    p.min = 0;
                } else
                {
                    p.min = float.Parse(l.ElementAt(2));
                }

                if (l.ElementAt(3) == "")
                {
                    p.max = 0;
                }
                else 
                {
                    p.max = float.Parse(l.ElementAt(3));
                }

                a.parametroIntervaloNumerico.Add(p);

            }
            context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            Alerta a = context.Alertas.Find(id);
            for (int i = 0; i < a.fixo.Count(); i++)
            {
                ParametroFixo p = context.ParametroFixoes.Find(a.fixo.ElementAt(i).ID);
                context.ParametroFixoes.Remove(p);
            }

            for (int i = 0; i < a.parametroAlfaNumerico.Count(); i++)
            {
                ParametroConstantesAlfaNumerico p = context.ParametroConstantesAlfaNumericoes.Find(a.parametroAlfaNumerico.ElementAt(i).ID);
                context.ParametroConstantesAlfaNumericoes.Remove(p);
            }

            for (int i = 0; i < a.parametroIntervaloNumerico.Count(); i++)
            {
                ParametroConstantesIntervaloNumerico p = context.ParametroConstantesIntervaloNumericoes.Find(a.parametroIntervaloNumerico.ElementAt(i).ID);
                context.ParametroConstantesIntervaloNumericoes.Remove(p);
            }

            context.Alertas.Remove(a);
            context.SaveChanges();
            return true;
        }
    }
}
