﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class Modelo3DRepository : IRepository<Modelo3DDTO>
    {
        public ImoveisDbContext context;

        public Modelo3DRepository()
        {
            context = new ImoveisDbContext();
        }

        public Modelo3DDTO Create(Modelo3DDTO tipo)
        {
            Modelo3D m = new Modelo3D();
            m.Content = System.Text.Encoding.UTF8.GetBytes(tipo.Content);
            m.ContentType = tipo.ContentType;

            context.Modelos3D.Add(m);
            context.SaveChanges();
            return tipo;
        }

        public bool Delete(int id)
        {
            var m = context.Modelos3D.Find(id);
            if (m != null)
            {
                context.Modelos3D.Remove(m);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Modelo3DDTO> GetData()
        {
            List<Modelo3D> modelos3D = context.Modelos3D.ToList();
            List<Modelo3DDTO> dto = new List<Modelo3DDTO>();
            foreach (Modelo3D m in modelos3D)
            {
                Modelo3DDTO tmp = new Modelo3DDTO(m);
                dto.Add(tmp);

            }
            return dto;
        }

        public Modelo3DDTO GetData(int id)
        {
            return new Modelo3DDTO(context.Modelos3D.Find(id));
        }

        public bool Update(int id, Modelo3DDTO t)
        {
            var m = context.Modelos3D.Find(id);
            if (t != null)
            {
                m.Content = System.Text.Encoding.UTF8.GetBytes(t.Content);
                m.ContentType = t.ContentType;
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
