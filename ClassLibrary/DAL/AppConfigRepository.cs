﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class AppConfigRepository : IRepository<AppConfigDTO>
    {
        public ImoveisDbContext context;

        public AppConfigRepository()
        {
            context = new ImoveisDbContext();
        }

        public AppConfigDTO Create(AppConfigDTO cfg)
        {
            AppConfig a = new AppConfig();
            a.IntervaloAlertas = cfg.IntervaloAlertas;
            a.IntervaloCorresp = cfg.IntervaloCorresp;

            if (cfg.Content != null)
            {
                a.Content = System.Text.Encoding.Default.GetBytes(cfg.Content);
                a.ContentType = cfg.ContentType;
            }

            context.AppConfigs.Add(a);
            context.SaveChanges();
            return cfg;
        }

        public bool Delete(int id)
        {
            var a = context.AppConfigs.Find(id);
            if (a != null)
            {
                context.AppConfigs.Remove(a);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<AppConfigDTO> GetData()
        {
            List<AppConfig> cfgs = context.AppConfigs.ToList();
            List<AppConfigDTO> dto = new List<AppConfigDTO>();
            foreach (AppConfig a in cfgs)
            {
                AppConfigDTO tmp = new AppConfigDTO(a);
                dto.Add(tmp);
            }
            return dto;
        }

        public AppConfigDTO GetData(int id)
        {
            return new AppConfigDTO(context.AppConfigs.Find(id));
        }

        public bool Update(int id, AppConfigDTO cfg)
        {
            var a = context.AppConfigs.Find(id);
            if (a != null)
            {
                a.IntervaloAlertas = cfg.IntervaloAlertas;
                a.IntervaloCorresp = cfg.IntervaloCorresp;
                if (cfg.Content != null)
                {
                    a.Content = System.Text.Encoding.Default.GetBytes(cfg.Content);
                    a.ContentType = cfg.ContentType;
                }
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
