﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class Imovel_Venda_Permuta_AluguerRepository : IRepository<Imovel_Venda_Permuta_AluguerDTO>
    {
        public ImoveisDbContext context;

        public Imovel_Venda_Permuta_AluguerRepository()
        {
            context = new ImoveisDbContext();
        }

        public Imovel_Venda_Permuta_AluguerDTO Create(Imovel_Venda_Permuta_AluguerDTO imovel)
        {
            Imovel_Venda_Permuta_Aluguer tmp = new Imovel_Venda_Permuta_Aluguer();
            tmp.TipoImovelID = imovel.tipoImovelID;
            tmp.tipo_de_imovel = context.TipoImovels.Find(imovel.tipoImovelID);

            Localizacao l = new Localizacao() { latitude = imovel.latitude, local = imovel.local, longitude = imovel.longitude };
            tmp.localizacao = l;

            tmp.area = imovel.area;
            tmp.Fotos = new List<Foto>();
            if (imovel.fotoContent != null)
            {
                tmp.Fotos.Add(new Foto());
                tmp.Fotos.First().Content = System.Text.Encoding.Default.GetBytes(imovel.fotoContent);
                tmp.Fotos.First().ContentType = imovel.fotoContentType;
            }

            tmp.Modelos3D = new List<Modelo3D>();
            if (imovel.modelo3DContent != null)
            {
                tmp.Modelos3D.Add(new Modelo3D());
                tmp.Modelos3D.First().Content = System.Text.Encoding.Default.GetBytes(imovel.modelo3DContent);
                tmp.Modelos3D.First().ContentType = imovel.modelo3DContentType;
            }

            tmp.ApplicationUserID = imovel.applicationUserID;

            tmp.PontosInteresse = new List<PontoInteresse>();
            if (imovel.pontoInteresse != null)
            {
                tmp.PontosInteresse.Add(new PontoInteresse());
                tmp.PontosInteresse.First().pontoInteresse = imovel.pontoInteresse;
            }

            HorarioVisita horario = new HorarioVisita()
            {
                HoraAbertura = imovel.HoraAbertura,
                HoraFecho = imovel.HoraFecho,
                DiasSemana = imovel.DiasSemana,
                DuraçãoVisita = imovel.DuraçãoVisita
            };
            tmp.horario = horario;

            context.Imovel_Venda_Permuta_Aluguer.Add(tmp);
            context.SaveChanges();
            return imovel;
        }

        public bool Delete(int id)
        {
            var imovel = context.Imovel_Venda_Permuta_Aluguer.Find(id);
            if (imovel != null)
            {
                context.Imovel_Venda_Permuta_Aluguer.Remove(imovel);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Imovel_Venda_Permuta_AluguerDTO> GetData()
        {
            List<Imovel_Venda_Permuta_Aluguer> imoveis = context.Imovel_Venda_Permuta_Aluguer.ToList();
            List<Imovel_Venda_Permuta_AluguerDTO> dto = new List<Imovel_Venda_Permuta_AluguerDTO>();
            foreach (Imovel_Venda_Permuta_Aluguer tmp in imoveis)
            {
                Imovel_Venda_Permuta_AluguerDTO imovel = new Imovel_Venda_Permuta_AluguerDTO(tmp);
                dto.Add(imovel);
            }
            return dto;
        }

        public Imovel_Venda_Permuta_AluguerDTO GetData(int id)
        {
            return new Imovel_Venda_Permuta_AluguerDTO(context.Imovel_Venda_Permuta_Aluguer.Find(id));
        }

        public bool Update(int id, Imovel_Venda_Permuta_AluguerDTO t)
        {
            var imovel = context.Imovel_Venda_Permuta_Aluguer.Find(id);
            if (imovel != null)
            {
                imovel.area = t.area;
                Localizacao l = context.Localizacaos.Find(imovel.localizacaoID);
                l.local = t.local;
                l.longitude = t.longitude;
                l.latitude = t.latitude;
                imovel.TipoImovelID = t.tipoImovelID;
                imovel.tipo_de_imovel = context.TipoImovels.Find(t.tipoImovelID);
                imovel.Fotos = new List<Foto>();
                if (t.fotoContent != null)
                {
                    imovel.Fotos.Add(new Foto());
                    imovel.Fotos.First().Content = System.Text.Encoding.Default.GetBytes(t.fotoContent);
                    imovel.Fotos.First().ContentType = t.fotoContentType;
                }

                if (t.pontoInteresse != null)
                {
                    imovel.PontosInteresse.Add(new PontoInteresse());
                    imovel.PontosInteresse.First().pontoInteresse = t.pontoInteresse;
                }

                HorarioVisita horario = context.HorariosVisita.Find(imovel.HorarioVisitaID);
                horario.HoraAbertura = t.HoraAbertura;
                horario.HoraFecho = t.HoraFecho;
                horario.DiasSemana = t.DiasSemana;
                horario.DuraçãoVisita = t.DuraçãoVisita;

                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
