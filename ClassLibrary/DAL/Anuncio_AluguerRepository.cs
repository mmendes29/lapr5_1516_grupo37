﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public class Anuncio_AluguerRepository : IRepository<Anuncio_AluguerDTO>
    {
           public ImoveisDbContext context;

        public Anuncio_AluguerRepository()
        {
            context = new ImoveisDbContext();          
        }

        public Anuncio_AluguerDTO Create(Anuncio_AluguerDTO anuncio)
        {
            Anuncio_Aluguer t = new Anuncio_Aluguer();
            t.renda = anuncio.renda;
            t.ImovelID = anuncio.ImovelID;
            t.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
            t.ApplicationUserID = anuncio.applicationUserID;
            t.valido = "false";

            context.Anuncio_Aluguer.Add(t);
            context.SaveChanges();
            return anuncio;
        }

        public bool Delete(int id)
        {
            var anuncio = context.Anuncio_Aluguer.Find(id);
            if (anuncio != null)
            {
                context.Anuncio_Aluguer.Remove(anuncio);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Anuncio_AluguerDTO> GetData()
        {
            List<Anuncio_Aluguer> anuncio = context.Anuncio_Aluguer.ToList();
            List<Anuncio_AluguerDTO> dto = new List<Anuncio_AluguerDTO>();
            foreach (Anuncio_Aluguer t in anuncio)
            {
                Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(t.ImovelID);
                Anuncio_AluguerDTO tmp = new Anuncio_AluguerDTO(t,i);
                dto.Add(tmp);
                
            }
            return dto;
        }

        public Anuncio_AluguerDTO GetData(int id)
        {
            Anuncio_Aluguer a = context.Anuncio_Aluguer.Find(id);
            Imovel_Venda_Permuta_Aluguer i = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
            return new Anuncio_AluguerDTO(a, i);
        }

        public bool Update(int id, Anuncio_AluguerDTO a)
        {
            var anuncio = context.Anuncio_Aluguer.Find(id);
            if (a != null)
            {
                anuncio.renda = a.renda;
                anuncio.ImovelID = a.ImovelID;
                anuncio.Imovel = context.Imovel_Venda_Permuta_Aluguer.Find(a.ImovelID);
                anuncio.valido = a.valido;
                anuncio.MediadorID = a.MediadorID;
                anuncio.valido = a.valido;
                if(a.tipoImovelID != 0)
                {
                    var imovel = context.Imovel_Venda_Permuta_Aluguer.Find(anuncio.ImovelID);

                    Localizacao l = context.Localizacaos.Find(imovel.localizacaoID);
                    l.local = a.local;
                    l.longitude = a.longitude;
                    l.latitude = a.latitude;
                }
                context.SaveChanges();

                return true;
            }
            else return false;
        }
    }
}
