﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    public interface IRepository<T>
    {
        List<T> GetData();
        T GetData(int id);
        T Create(T obj);
        bool Update(int id, T obj);
        bool Delete(int id);
    }
}
