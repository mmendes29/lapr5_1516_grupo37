﻿using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class ImoveisInitializer : DropCreateDatabaseIfModelChanges<ImoveisDbContext>
    {
        protected override void Seed(ImoveisDbContext context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEF(ImoveisDbContext db)
        {

        }
    }
}
