﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class PedidoVisitasRepository : IRepository<PedidoVisitaDTO>
    {
        public ImoveisDbContext context;

        public PedidoVisitasRepository()
        {
            context = new ImoveisDbContext();
        }

        public PedidoVisitaDTO Create(PedidoVisitaDTO visita)
        {
            PedidoVisita m = new PedidoVisita();
            m.ClienteId = visita.ClienteId;
            m.MediadorId = visita.MediadorId;
            m.ImovelId = visita.ImovelId;
            m.Data = visita.Data;
            m.Hora = visita.Hora;
            m.Estado = visita.Estado;

            context.PedidosVisita.Add(m);
            context.SaveChanges();
            return visita;
        }

        public bool Delete(int id)
        {
            PedidoVisita a = context.PedidosVisita.Find(id);
            context.PedidosVisita.Remove(a);
            context.SaveChanges();
            return true;
        }

        public List<PedidoVisitaDTO> GetData()
        {
            List<PedidoVisita> visitas = context.PedidosVisita.ToList();
            List<PedidoVisitaDTO> dto = new List<PedidoVisitaDTO>();
            foreach (PedidoVisita m in visitas)
            {
                PedidoVisitaDTO tmp = new PedidoVisitaDTO(m);
                dto.Add(tmp);
            }
            return dto;
        }

        public PedidoVisitaDTO GetData(int id)
        {
            return new PedidoVisitaDTO(context.PedidosVisita.Find(id));
        }

        public bool Update(int id, PedidoVisitaDTO visita)
        {
            PedidoVisita p = context.PedidosVisita.Find(id);
            p.ClienteId = visita.ClienteId;
            p.MediadorId = visita.MediadorId;
            p.ImovelId = visita.ImovelId;
            p.Data = visita.Data;
            p.Hora = visita.Hora;
            p.Estado = visita.Estado;

            context.SaveChanges();
            return true;
        }
    }
}
