﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class AnuncioAlertaRepository : IRepository<AnuncioAlertaDTO>
    {
        public ImoveisDbContext context;

        public AnuncioAlertaRepository()
        {
            context = new ImoveisDbContext();
        }

        public AnuncioAlertaDTO Create(AnuncioAlertaDTO a)
        {
            AnuncioAlerta tmp = new AnuncioAlerta();
            tmp.AlertaId = a.AlertaId;
            tmp.AnuncioId = a.AnuncioId;

            context.AnunciosAlerta.Add(tmp);
            context.SaveChanges();
            return a;
        }

        public bool Delete(int id)
        {
            var a = context.AnunciosAlerta.Find(id);
            if (a != null)
            {
                context.AnunciosAlerta.Remove(a);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<AnuncioAlertaDTO> GetData()
        {
            List<AnuncioAlerta> anunciosEmAlerta = context.AnunciosAlerta.ToList();
            List<AnuncioAlertaDTO> dto = new List<AnuncioAlertaDTO>();
            foreach (AnuncioAlerta a in anunciosEmAlerta)
            {
                AnuncioAlertaDTO tmp = new AnuncioAlertaDTO(a);
                dto.Add(tmp);
            }
            return dto;
        }

        public AnuncioAlertaDTO GetData(int id)
        {
            return new AnuncioAlertaDTO(context.AnunciosAlerta.Find(id));
        }

        public bool Update(int id, AnuncioAlertaDTO a)
        {
            var tmp = context.AnunciosAlerta.Find(id);
            if (tmp != null)
            {
                tmp.AlertaId = a.AlertaId;
                tmp.AnuncioId = a.AnuncioId;

                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
