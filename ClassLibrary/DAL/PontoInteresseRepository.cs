﻿using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary.DAL
{
    public class PontoInteresseRepository : IRepository<PontoInteresseDTO>
    {
        public ImoveisDbContext context;

        public PontoInteresseRepository()
        {
            context = new ImoveisDbContext();
        }

        public PontoInteresseDTO Create(PontoInteresseDTO pontoInteresse)
        {
            PontoInteresse p = new PontoInteresse();
            p.pontoInteresse = pontoInteresse.pontoInteresse;

            context.PontosInteresse.Add(p);
            context.SaveChanges();
            return pontoInteresse;
        }

        public bool Delete(int id)
        {
            var p = context.PontosInteresse.Find(id);
            if (p != null)
            {
                context.PontosInteresse.Remove(p);
                context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<PontoInteresseDTO> GetData()
        {
            List<PontoInteresse> pontosInteresse = context.PontosInteresse.ToList();
            List<PontoInteresseDTO> dto = new List<PontoInteresseDTO>();
            foreach(PontoInteresse p in pontosInteresse)
            {
                PontoInteresseDTO tmp = new PontoInteresseDTO(p);
                dto.Add(tmp);
            }
            return dto;
        }

        public PontoInteresseDTO GetData(int id)
        {
            return new PontoInteresseDTO(context.PontosInteresse.Find(id));
        }

        public bool Update(int id, PontoInteresseDTO p)
        {
            var pontoInteresse = context.PontosInteresse.Find(id);
            if (p != null)
            {
                pontoInteresse.pontoInteresse = p.pontoInteresse;
                context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
