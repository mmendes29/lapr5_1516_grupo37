namespace ClassLibrary.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<ClassLibrary.DAL.ImoveisDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ClassLibrary.DAL.ImoveisDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (!context.Users.Any(t => t.UserName == "admin@isep.ipp.pt"))
            {
                var appConfig = new AppConfig { IntervaloAlertas = 10, IntervaloCorresp = 30 };
                var user = new ApplicationUser { UserName = "admin@isep.ipp.pt", Email = "admin@isep.ipp.pt" };
                userManager.Create(user, "Teste.123");

                context.AppConfigs.Add(appConfig);
                context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Admin" });
                context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Cliente" });
                context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Mediador" });
                context.SaveChanges();

                userManager.AddToRole(user.Id, "Admin");

            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
