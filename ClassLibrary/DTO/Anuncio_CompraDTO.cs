﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class Anuncio_CompraDTO
    {
        public int ID { get; set; }
        public int ImovelID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float areaMin { get; set; }
        public float areaMax { get; set; }
        public float precoMin { get; set; }
        public float precoMax { get; set; }
        public string valido { get; set; }
        public string correspValidada { get; set; }
        public string MediadorID { get; set; }
        public int AnuncioVendaID { get; set; }
        
        public Anuncio_CompraDTO ()
        {

        }
        public Anuncio_CompraDTO(Anuncio_Compra anuncio)
        {
            ID = anuncio.ID;
            ImovelID = anuncio.ImovelID;
            applicationUserID = anuncio.ApplicationUserID;
            tipoImovelID = anuncio.Imovel.TipoImovelID;
            tipoImovel = anuncio.Imovel.tipo_de_imovel.nome;
            localizacaoID = anuncio.Imovel.localizacaoID;
            local = anuncio.Imovel.localizacao.local;
            latitude = anuncio.Imovel.localizacao.latitude;
            longitude = anuncio.Imovel.localizacao.longitude;
            areaMin = anuncio.Imovel.areaMin;
            areaMax = anuncio.Imovel.areaMax;
            precoMin = anuncio.precoMin;
            precoMax = anuncio.precoMax;
            valido = anuncio.valido;
            correspValidada = anuncio.correspValidada;
            MediadorID = anuncio.MediadorID;
            AnuncioVendaID = anuncio.AnuncioVendaID;
        }

    }
}
