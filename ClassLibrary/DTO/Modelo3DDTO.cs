﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DTO
{
    public class Modelo3DDTO
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }

        public Modelo3DDTO()
        {

        }

        public Modelo3DDTO(Modelo3D f)
        {
            ID = f.ID;
            Content = System.Text.Encoding.Default.GetString(f.Content);
            ContentType = f.ContentType;
        }
    }
}
