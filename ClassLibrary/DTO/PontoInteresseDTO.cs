﻿using ClassLibrary.Models;

namespace ClassLibrary.DTO
{
    public class PontoInteresseDTO
    {
        public int PontoInteresseId { get; set; }
        public string pontoInteresse { get; set; }

        public PontoInteresseDTO()
        {

        }

        public PontoInteresseDTO(PontoInteresse p)
        {
            PontoInteresseId = p.PontoInteresseId;
            pontoInteresse = p.pontoInteresse;
        }
    }
}
