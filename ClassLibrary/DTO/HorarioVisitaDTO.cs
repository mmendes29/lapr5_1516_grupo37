﻿using ClassLibrary.Models;

namespace ClassLibrary.DTO
{
    public class HorarioVisitaDTO
    {
        public int HorarioVisitaId { get; set; }
        public int HoraAbertura { get; set; }
        public int HoraFecho { get; set; }
        public string DiasSemana { get; set; }
        public int DuraçãoVisita { get; set; }

        public HorarioVisitaDTO()
        {

        }

        public HorarioVisitaDTO(HorarioVisita horario)
        {
            HorarioVisitaId = horario.HorarioVisitaId;
            HoraAbertura = horario.HoraAbertura;
            HoraFecho = horario.HoraFecho;
            DiasSemana = horario.DiasSemana;
            DuraçãoVisita = horario.DuraçãoVisita;
        }
    }
}
