﻿using ClassLibrary.Models;

namespace ClassLibrary.DTO
{
    public class AppConfigDTO
    {
        public int AppConfigId { get; set; }
        public int IntervaloAlertas { get; set; }
        public int IntervaloCorresp { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }

        public AppConfigDTO()
        {

        }

        public AppConfigDTO(AppConfig a)
        {
            AppConfigId = a.AppConfigId;
            IntervaloAlertas = a.IntervaloAlertas;
            IntervaloCorresp = a.IntervaloCorresp;
            if (a.Content != null)
            {
                Content = System.Text.Encoding.Default.GetString(a.Content);
                ContentType = a.ContentType;          
            }
        }
    }
}
