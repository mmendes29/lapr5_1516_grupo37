﻿using ClassLibrary.Models;

namespace ClassLibrary.DTO
{
    public class Imovel_CompraDTO
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float areaMin { get; set; }
        public float areaMax { get; set; }

        public Imovel_CompraDTO ()
        {

        }

        public Imovel_CompraDTO(Imovel_Compra imovel)
        {
            ID = imovel.ID;
            applicationUserID = imovel.ApplicationUserID;
            tipoImovelID = imovel.TipoImovelID;
            tipoImovel = imovel.tipo_de_imovel.nome;
            localizacaoID = imovel.localizacaoID;
            local = imovel.localizacao.local;
            latitude = imovel.localizacao.latitude;
            longitude = imovel.localizacao.longitude;
            areaMin = imovel.areaMin;
            areaMax = imovel.areaMax;
        }
    }
}
