﻿using ClassLibrary.Models;

namespace ClassLibrary.DTO
{
    public class AnuncioAlertaDTO
    {
        public int AnuncioAlertaId { get; set; }
        public int AlertaId { get; set; }
        public int AnuncioId { get; set; }

        public AnuncioAlertaDTO()
        {

        }

        public AnuncioAlertaDTO(AnuncioAlerta a)
        {
            AnuncioAlertaId = a.AnuncioAlertaId;
            AlertaId = a.AlertaId;
            AnuncioId = a.AnuncioId;
        }
    }
}
