﻿using ClassLibrary.Models;
using System;

namespace ClassLibrary.DTO
{
    public class PedidoVisitaDTO
    {
        public int PedidoVisitaId { get; set; }
        public string ClienteId { get; set; }
        public string MediadorId { get; set; }
        public int ImovelId { get; set; }
        public DateTime Data { get; set; }
        public DateTime Hora { get; set; }
        public string Estado { get; set; }

        public PedidoVisitaDTO()
        {

        }

        public PedidoVisitaDTO(PedidoVisita p)
        {
            PedidoVisitaId = p.PedidoVisitaId;
            ClienteId = p.ClienteId;
            MediadorId = p.MediadorId;
            ImovelId = p.ImovelId;
            Data = p.Data;
            Hora = p.Hora;
            Estado = p.Estado;
        }
    }
}
