﻿using ClassLibrary.Models;
using System.Linq;

namespace ClassLibrary.DTO
{
    public class Imovel_Venda_Permuta_AluguerDTO
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float area { get; set; }
        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
        public int modelo3DID { get; set; }
        public string modelo3DContent { get; set; }
        public string modelo3DContentType { get; set; }
        public int PontoInteresseId { get; set; }
        public string pontoInteresse { get; set; }
        public int HorarioVisitaID { get; set; }
        public int HoraAbertura { get; set; }
        public int HoraFecho { get; set; }
        public string DiasSemana { get; set; }
        public int DuraçãoVisita { get; set; }

        public Imovel_Venda_Permuta_AluguerDTO ()
        {

        }
        public Imovel_Venda_Permuta_AluguerDTO (Imovel_Venda_Permuta_Aluguer imovel)
        {
            ID = imovel.ID;
            applicationUserID = imovel.ApplicationUserID;
            tipoImovelID = imovel.TipoImovelID;
            tipoImovel = imovel.tipo_de_imovel.nome;
            localizacaoID = imovel.localizacaoID;
            local = imovel.localizacao.local;
            latitude = imovel.localizacao.latitude;
            longitude = imovel.localizacao.longitude;
            area = imovel.area;
            if(imovel.Fotos.Count != 0)
            {
                fotoContent = System.Text.Encoding.UTF8.GetString(imovel.Fotos.First().Content, 0, imovel.Fotos.First().Content.Length);
                fotoContentType = imovel.Fotos.First().ContentType;
                fotoID = imovel.Fotos.First().ID;
            }

            if (imovel.Modelos3D.Count != 0)
            {
                modelo3DContent = System.Text.Encoding.UTF8.GetString(imovel.Modelos3D.First().Content, 0, imovel.Modelos3D.First().Content.Length);
                modelo3DContentType = imovel.Modelos3D.First().ContentType;
                modelo3DID = imovel.Modelos3D.First().ID;
            }

            if (imovel.PontosInteresse.Count != 0)
            {
                pontoInteresse = imovel.PontosInteresse.First().pontoInteresse;
                PontoInteresseId = imovel.PontosInteresse.First().PontoInteresseId;
            }

            HorarioVisitaID = imovel.HorarioVisitaID;
            HoraAbertura = imovel.horario.HoraAbertura;
            HoraFecho = imovel.horario.HoraFecho;
            DiasSemana = imovel.horario.DiasSemana;
            DuraçãoVisita = imovel.horario.DuraçãoVisita;
        }
    }
}
