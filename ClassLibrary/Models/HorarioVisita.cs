﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class HorarioVisita
    {
        [HiddenInput(DisplayValue = false)]
        public int HorarioVisitaId { get; set; }

        [Range(8, 19, ErrorMessage = "Insira um valor válido (8 a 19)"), DisplayName("Hora de abertura")]
        public int HoraAbertura { get; set; }

        [Range(9, 20, ErrorMessage = "Insira um valor válido (9 a 20)"), DisplayName("Hora de fecho")]
        public int HoraFecho { get; set; }

        [DisplayName("Dias da semana")]
        public string DiasSemana { get; set; }

        [DisplayName("Duração da visita")]
        public int DuraçãoVisita { get; set; }
    }
}
