﻿using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class AnuncioAlerta
    {
        [HiddenInput(DisplayValue = false)]
        public int AnuncioAlertaId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AlertaId { get; set; }

        public virtual Alerta alerta { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AnuncioId { get; set; }

        public virtual Anuncio anuncio { get; set; }
    }
}
