﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class Localizacao
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        [Required, DisplayName("Localização")]
        public string local { get; set; }

        [Required, Range(-90, 90, ErrorMessage = "Insira um valor válido (-90 a 90 graus)"),DisplayName("Latitude (em graus)")]
        public string latitude { get; set; }

        [Required, Range(-180, 180, ErrorMessage = "Insira um valor válido (-180 a 180 graus)"), DisplayName("Longitude (em graus)")]
        public string longitude { get; set; }
    }
}