﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class TipoImovel
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        [Required, DisplayName("Tipo de Imóvel")]
        public string nome { get; set; }
        
        public virtual TipoImovel subTipo { get; set; }
    }
}