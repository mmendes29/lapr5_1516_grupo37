﻿using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class PontoInteresse
    {
        [HiddenInput(DisplayValue = false)]
        public int PontoInteresseId { get; set; }
        public string pontoInteresse { get; set; }
        public int Imovel_Venda_Permuta_AluguerID { get; set; }
        public virtual Imovel_Venda_Permuta_Aluguer imovel { get; set; }
    }
}
