﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClassLibrary.Models
{
    public class AppConfig
    {
        [HiddenInput(DisplayValue = false)]
        public int AppConfigId { get; set; }

        public int IntervaloAlertas { get; set; }

        public int IntervaloCorresp { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        public byte[] Content { get; set; }

        [StringLength(100)]
        public string ContentType { get; set; }
    }
}
