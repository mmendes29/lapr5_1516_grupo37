﻿using System.ComponentModel.DataAnnotations;

namespace ClassLibrary.Models
{
    public class Foto
    {
        public int ID { get; set; }
        [StringLength(255)]
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public FileType FileType { get; set; }
        public int Imovel_Venda_Permuta_AluguerID { get; set; }
        public virtual Imovel_Venda_Permuta_Aluguer imovel { get; set; }
    }
}