%imovel(Nmr,Nome,PosX,PosY,DuracaoVisita,Abertura,Fecho)
%Simula��o de mapa igual a aula TP
imovel(a,ramalde,45,95,15,540,1020).
imovel(b,maia,90,95,10,540,1080).
imovel(c,matosinhos,15,85,20,540,1140).
imovel(d,famalicao,70,80, 50, 100, 200).


%estrada(Imovel1,Imovel2,Distancia).
estrada(a,b,10).
estrada(b,a,10).
estrada(a,c,12).
estrada(c,a,12).
estrada(b,c,8).
estrada(c,b,8).
estrada(c,d,3).
%estrada(a,d,15).
%estrada(d,a,15).

%%	Requisito LAPR5
%Numa determinada hora, dada uma lista de imoveis a visitar, mostrar a melhor alternativa, considerando o horario das visitas
%dos im�veis e o tempo total de cada visita e tb o tempo gasto nas desloca��es
% Consideramos que o mediador come�a num im�vel e s� sai no final da visita.


%estimativa
estimativa(I1,I2,Est):-
	imovel(I1,_,X1,Y1,_,_,_),
	imovel(I2,_,X2,Y2,_,_,_),
	DX is X1-X2,
	DY is Y1-Y2,
	Est is sqrt(DX*DX+DY*DY).

	melhor([H],H, []).
melhor([H|T],M,[H|L2]):-
	melhor(T,M,L2),
	melhor2(M, H), !.
melhor([H|T],H, T).

melhor2((C1,C2,_), (C3,C4,_)) :-
		T1 is C1 + C2,
		T2 is C3 + C4,
		T1 < T2.

a_starRecursivo([(TempoViagem,_,[Destino|T])|_],Destino,[Destino|T],TempoViagem).
a_starRecursivo([(TempoViagem,_,[H|T])|L],Destino,Caminho,TempoViagemFinal):-
	findall((TempoViagem2,X,[Y,H|T]),((estrada(H,Y,Distancia);estrada(Y,H,Distancia)),
					  not(member(Y,T)),
					  TempoViagem2 is TempoViagem + Distancia,
					  estimativa(Y,Destino,X)),L1),
	append(L,L1,L2),
	melhor(L2,Melhor,L3),
	a_starRecursivo([Melhor|L3],Destino,Caminho,TempoViagemFinal),!.


a_star(Origem, Destino, CaminhoFinal, TempoViagem):-
	estimativa(Origem, Destino, Estimativa),
	a_starRecursivo([(0,Estimativa,[Origem])],Destino,Caminho,TempoViagem),
	reverse(Caminho,CaminhoFinal).
