﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClienteAPP.Startup))]
namespace ClienteAPP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
