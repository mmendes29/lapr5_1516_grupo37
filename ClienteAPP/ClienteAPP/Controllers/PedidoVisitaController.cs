﻿using System.Web.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using ClienteAPP.Models;
using System.Net;
using System.Linq;
using System;
using Newtonsoft.Json;
using ClienteAPP.Helpers;
using System.Net.Http;

namespace ClienteAPP.Controllers
{
    public class PedidoVisitaController : Controller
    {
        // GET: PedidoVisita
        public async Task<ActionResult> Index()
        {
            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            IEnumerable<PedidoVisita> visitas = await getAll();
            if (visitas != null)
            {
                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                List<Imovel_Venda_Permuta_Aluguer> imoveis = new List<Imovel_Venda_Permuta_Aluguer>();
                foreach (PedidoVisita visita in visitas)
                    imoveis.Add(await ImovelCtrl.get(visita.ImovelId));

                ViewBag.visitas = visitas;
                ViewBag.imoveis = imoveis;
            }
            else
            {
                ViewBag.visitas = new List<PedidoVisita>();
                ViewBag.imoveis = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            return View();
        }

        public async Task<IEnumerable<PedidoVisita>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/PedidoVisita/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visitas = JsonConvert.DeserializeObject<IEnumerable<PedidoVisita>>(content);
                return visitas;
            }
            else
                return null;
        }

        // GET: PedidoVisita/Create/id
        public async Task<ActionResult> Create(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
            Anuncio_VendaController AnuncioVendaCtrl = new Anuncio_VendaController();
            IEnumerable<Anuncio_Venda> AnuncioVendaList = await AnuncioVendaCtrl.getAll();
            if (AnuncioVendaList != null)
                foreach (Anuncio_Venda anuncio in AnuncioVendaList)
                    if (anuncio.ID == id)
                    {
                        Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncio.ImovelID);
                        ViewBag.imovel = imovel;
                        ViewBag.anuncio = anuncio;
                        PedidoVisita visita = new PedidoVisita { ImovelId = imovel.ID, MediadorId = anuncio.MediadorID };
                        return View(visita);
                    }

            Anuncio_AluguerController AnuncioAluguerCtrl = new Anuncio_AluguerController();
            IEnumerable<Anuncio_Aluguer> AnuncioAluguerList = await AnuncioAluguerCtrl.getAll();
            if (AnuncioAluguerList != null)
                foreach (Anuncio_Aluguer anuncio in AnuncioAluguerList)
                    if (anuncio.ID == id)
                    {
                        Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncio.ImovelID);
                        ViewBag.imovel = imovel;
                        ViewBag.anuncio = anuncio;
                        PedidoVisita visita = new PedidoVisita { ImovelId = imovel.ID, MediadorId = anuncio.MediadorID };
                        return View(visita);
                    }

            Anuncio_PermutaController AnuncioPermutaCtrl = new Anuncio_PermutaController();
            IEnumerable<Anuncio_Permuta> AnuncioPermutaList = await AnuncioPermutaCtrl.getAll();
            if (AnuncioPermutaList != null)
                foreach (Anuncio_Permuta anuncio in AnuncioPermutaList)
                    if (anuncio.ID == id)
                    {
                        Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncio.ImovelID);
                        ViewBag.imovel = imovel;
                        ViewBag.anuncio = anuncio;
                        PedidoVisita visita = new PedidoVisita { ImovelId = imovel.ID, MediadorId = anuncio.MediadorID };
                        return View(visita);
                    }

            return RedirectToAction("Erro", "Home");
        }

        // POST: PedidoVisita/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MediadorId, ImovelId, Hora, Data")] PedidoVisita visita)
        {
            try
            {
                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(visita.ImovelId);
                string diaSemana = visita.Data.DayOfWeek.ToString();
                List<string> diasDaSemana = imovel.DiasSemana.Split(';').ToList();
                foreach (string dia in diasDaSemana)
                    if (dia == diaSemana && imovel.HoraAbertura <= Convert.ToInt32(visita.Hora.Hour.ToString())
                        && imovel.HoraFecho >= Convert.ToInt32(visita.Hora.Hour.ToString()))
                    {
                        visita.Estado = "Por aprovar";
                        var client = WebApiHttpClient.GetClient();
                        string visitaJSON = JsonConvert.SerializeObject(visita);
                        HttpContent content = new StringContent(visitaJSON,
                        System.Text.Encoding.Unicode, "application/json");
                        var response = await client.PostAsync("api/PedidoVisita/", content);

                        if (response.IsSuccessStatusCode)
                            return RedirectToAction("Index", "Anuncios");
                        else
                            return RedirectToAction("Erro", "Home");
                    }
                return Content("ERRO: O imóvel não se encontra disponivel na data ou hora escolhida.");
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: PedidoVisita/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/PedidoVisita/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var visita = JsonConvert.DeserializeObject<PedidoVisita>(content);
                if (visita == null)
                    return HttpNotFound();

                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(visita.ImovelId);
                ViewBag.imovel = imovel;
                return View(visita);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: PedidoVisita/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PedidoVisitaId, ClienteId, MediadorId, ImovelId, Data, Hora, Estado")] PedidoVisita visita)
        {
            try
            {
                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(visita.ImovelId);
                string diaSemana = visita.Data.DayOfWeek.ToString();
                List<string> diasDaSemana = imovel.DiasSemana.Split(';').ToList();
                foreach (string dia in diasDaSemana)
                    if (dia == diaSemana && imovel.HoraAbertura <= Convert.ToInt32(visita.Hora.Hour.ToString())
                        && imovel.HoraFecho >= Convert.ToInt32(visita.Hora.Hour.ToString()))
                    {
                        var client = WebApiHttpClient.GetClient();
                        string pedidoVisitaJSON = JsonConvert.SerializeObject(visita);
                        HttpContent content = new StringContent(pedidoVisitaJSON,
                        System.Text.Encoding.Unicode, "application/json");
                        var response = await client.PutAsync("api/PedidoVisita/" + visita.PedidoVisitaId, content);

                        if (response.IsSuccessStatusCode)
                            return RedirectToAction("Index");
                        else
                            return RedirectToAction("UnAuthenticated", "Home");
                    }
                return Content("ERRO: O imóvel não se encontra disponivel na data ou hora escolhida.");
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: PedidoVisita/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/PedidoVisita/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                PedidoVisita visita = JsonConvert.DeserializeObject<PedidoVisita>(content);
                if (visita == null)
                    return HttpNotFound();

                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(visita.ImovelId);
                ViewBag.imovel = imovel;
                return View(visita);
            }
            else
                return RedirectToAction("UnAuthenticated", "Home");
        }

        // GET: PedidoVisita/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/PedidoVisita/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                PedidoVisita visita = JsonConvert.DeserializeObject<PedidoVisita>(content);
                if (visita == null)
                    return HttpNotFound();

                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(visita.ImovelId);
                ViewBag.imovel = imovel;
                return View(visita);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: PedidoVisita/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/PedidoVisita/" + id);
                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index");

                else
                    return RedirectToAction("UnAuthenticated", "Home");
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
    }
}
