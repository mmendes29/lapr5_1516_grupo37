﻿using ClienteAPP.Helpers;
using ClienteAPP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClienteAPP.Controllers
{
    public class AlertasController : Controller
    {
        // GET: Alertas
        public async Task<ActionResult> Index()
        {
            if (WebApiHttpClient.getToken() == null)
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }

            IEnumerable<Alerta> list = await getAll();
            if (list != null)
            {
                ViewBag.Alertas = list;
            }
            else
            {
                ViewBag.Alertas = new List<Alerta>();
            }
            return View();
        }

        public async Task<IEnumerable<Alerta>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alertas/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta =
                JsonConvert.DeserializeObject<IEnumerable<Alerta>>(content);
                return alerta;
            }
            else
            {
                return null;
            }
        }

        public async Task<Alerta> get(int Id)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alertas/" + Id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                return alerta;
            }
            else
            {
                return null;
            }
        }

        // GET: Alertas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alertas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();
                return View(alerta);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Alertas/Create
        public async Task<ActionResult> Create()
        {
            List<string> tiposAnuncios = new List<string>();
            tiposAnuncios.Add("Compra");
            tiposAnuncios.Add("Aluguer");
            tiposAnuncios.Add("Permuta");
            tiposAnuncios.Add("Venda");
            ViewBag.TipoAnuncios = tiposAnuncios;

            List<string> tipoImoveis = new List<string>();

            TipoImovelController ctrl = new TipoImovelController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<TipoImovel>();
            }

            foreach (var item in list)
            {
                tipoImoveis.Add(item.nome);
            }

            ViewBag.tipoImoveis = tipoImoveis;
            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID")] Alerta alerta, string local, string areaMin, string areaMax, string precoMin, string precoMax, string[] TipoAnuncio, string[] TipoImovel)
        {
            try
            {

                alerta.parametroAlfaNumerico = new List<List<string>>();
                alerta.parametroIntervaloNumerico = new List<List<string>>();
                alerta.fixo = new List<List<string>>();

                if (TipoAnuncio != null && TipoAnuncio.Length != 0)
                {
                    foreach (var item in TipoAnuncio)
                    {
                        List<string> tmp = new List<string>();
                        tmp.Add("0");
                        tmp.Add("TipoAnuncio");
                        tmp.Add(item);

                        alerta.fixo.Add(tmp);
                    }
                }

                if (TipoImovel != null && TipoImovel.Length != 0)
                {
                    foreach (var item in TipoImovel)
                    {
                        List<string> tmp = new List<string>();
                        tmp.Add("0");
                        tmp.Add("TipoImovel");
                        tmp.Add(item);

                        alerta.fixo.Add(tmp);
                    }
                }

                if (local.Length != 0)
                {
                    List<string> tmp = new List<string>();
                    tmp.Add("0");
                    tmp.Add("local");
                    tmp.Add(local);

                    alerta.parametroAlfaNumerico.Add(tmp);
                }

                if (areaMin.Length != 0 || areaMax.Length != 0)
                {

                    List<string> tmp = new List<string>();
                    tmp.Add("0");
                    tmp.Add("area");

                    tmp.Add("");
                    tmp.Add("");

                    if (areaMin.Length != 0) tmp[2] = areaMin;
                    if (areaMax.Length != 0) tmp[3] = areaMax;

                    alerta.parametroIntervaloNumerico.Add(tmp);
                }


                if (precoMin.Length != 0 || precoMax.Length != 0)
                {

                    List<string> tmp = new List<string>();
                    tmp.Add("0");
                    tmp.Add("preco");

                    tmp.Add("");
                    tmp.Add("");

                    if (precoMin.Length != 0) tmp[2] = precoMin;
                    if (precoMax.Length != 0) tmp[3] = precoMax;

                    alerta.parametroIntervaloNumerico.Add(tmp);
                }

                var client = WebApiHttpClient.GetClient();
                string alertaJSON = JsonConvert.SerializeObject(alerta);
                HttpContent content = new StringContent(alertaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Alertas", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Alertas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alertas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();

                List<string> tiposAnuncios = new List<string>();
                tiposAnuncios.Add("Compra");
                tiposAnuncios.Add("Aluguer");
                tiposAnuncios.Add("Permuta");
                tiposAnuncios.Add("Venda");
                ViewBag.TipoAnuncios = tiposAnuncios;

                List<string> tiposAnunciosJaSelecionados = new List<string>();
                List<List<string>> l = alerta.fixo;
                foreach (var item in l)
                {
                    if (item[1] == "TipoAnuncio")
                    {
                        tiposAnunciosJaSelecionados.Add(item[2]);
                    }

                }
                ViewBag.TiposAnunciosSelec = tiposAnunciosJaSelecionados;

                List<string> tipoImoveis = new List<string>();

                TipoImovelController ctrl = new TipoImovelController();
                IEnumerable<TipoImovel> list = await ctrl.getAll();
                foreach (var item in list.ToList())
                {
                    tipoImoveis.Add(item.nome);
                }

                ViewBag.tipoImoveis = tipoImoveis;

                List<string> tiposImoveisJaSelecionados = new List<string>();
                foreach (var item in l)
                {
                    if (item[1] == "TipoImovel")
                    {
                        tiposImoveisJaSelecionados.Add(item[2]);
                    }

                }
                ViewBag.TiposImovelSelec = tiposImoveisJaSelecionados;


                foreach (List<string> l1 in alerta.parametroAlfaNumerico)
                {
                    if (l1[1] == "local")
                    {
                        ViewBag.local = l1[2];
                    }
                }

                foreach (List<string> l1 in alerta.parametroIntervaloNumerico)
                {
                    if (l1[1] == "area")
                    {
                        ViewBag.areamin = l1[2];
                        ViewBag.areamax = l1[3];
                    }

                    if (l1[1] == "preco")
                    {
                        ViewBag.precomin = l1[2];
                        ViewBag.precomax = l1[3];
                    }
                }

                return View(alerta);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID")] Alerta alerta, string local, string areaMin, string areaMax, string precoMin, string precoMax, string[] TipoAnuncio, string[] TipoImovel)
        {
            try
            {
                alerta.parametroAlfaNumerico = new List<List<string>>();

                if (TipoAnuncio == null) TipoAnuncio = new string[0];
                if (TipoImovel == null) TipoImovel = new string[0];

                List<string> l = new List<string>();
                l.Add("0");
                l.Add("local");
                l.Add(local);

                alerta.parametroAlfaNumerico.Add(l);

                alerta.fixo = new List<List<string>>();

                foreach (string s in TipoAnuncio)
                {
                    l = new List<String>();
                    l.Add("0");
                    l.Add("TipoAnuncio");
                    l.Add(s);

                    alerta.fixo.Add(l);
                }

                foreach (string s in TipoImovel)
                {
                    l = new List<String>();
                    l.Add("0");
                    l.Add("TipoImovel");
                    l.Add(s);

                    alerta.fixo.Add(l);
                }

                alerta.parametroIntervaloNumerico = new List<List<string>>();

                l = new List<String>();
                l.Add("0");
                l.Add("area");
                l.Add(areaMin);
                l.Add(areaMax);

                alerta.parametroIntervaloNumerico.Add(l);

                l = new List<String>();
                l.Add("0");
                l.Add("preco");
                l.Add(precoMin);
                l.Add(precoMax);

                alerta.parametroIntervaloNumerico.Add(l);



                var client = WebApiHttpClient.GetClient();
                string alertaJSON = JsonConvert.SerializeObject(alerta);
                HttpContent content = new StringContent(alertaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Alertas/" + alerta.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Alertas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alertas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();
                return View(alerta);
            }
            return RedirectToAction("Erro", "Home");
        }

        // POST: Alertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Alertas/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
    }
}
