﻿using ClienteAPP.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClienteAPP.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public async Task<ActionResult> Index()
        {
            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Users/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var getUsers = JsonConvert.DeserializeObject<IEnumerable<string>>(content);

                List<string> usersId = new List<string>();
                foreach (var user in getUsers)
                    usersId.Add(user);

                List<string> usersEmail = new List<string>();
                foreach (string Id in usersId)
                {
                    response = await client.GetAsync("api/Users/" + Id);
                    content = await response.Content.ReadAsStringAsync();
                    var email = JsonConvert.DeserializeObject<string>(content);
                    usersEmail.Add(email.ToString());
                }

                List<string> userRoles = new List<string>();
                RoleController role = new RoleController();
                foreach (var user in usersId)
                {
                    List<string> roles = await role.GetUserRoles(user);
                    foreach (string r in roles)
                        userRoles.Add(r);
                }

                ViewBag.usersId = usersId;
                ViewBag.usersEmail = usersEmail;
                ViewBag.userRoles = userRoles;

                return View();
            }
            else
                return null;
        }
    }
}