﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAPP.Models;
using ClienteAPP.Helpers;


namespace ClienteAPP.Controllers
{
    public class Anuncio_AluguerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Anuncio_Aluguer
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Aluguer>>(content);
                List<Imovel_Venda_Permuta_Aluguer> list = new List<Imovel_Venda_Permuta_Aluguer>();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                foreach (Anuncio_Aluguer a in anunciosAluguer)
                {
                    list.Add(await ctrl.get(a.ImovelID));
                }
                ViewBag.Imoveis =  list;
                return View(anunciosAluguer);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        public async Task<IEnumerable<Anuncio_Aluguer>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Aluguer>>(content);
                return anunciosAluguer;
            }
            else
            {
                return null;
            }
        }

        // GET: Anuncio_Aluguer/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioAluguer = JsonConvert.DeserializeObject<Anuncio_Aluguer>(content);
                if (anuncioAluguer == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioAluguer.ImovelID);
                return View(anuncioAluguer);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Anuncio_Aluguer/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListImovel();
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            ViewBag.Imovel = list;
            return View();
        }

        // POST: Anuncio_Aluguer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,renda")] Anuncio_Aluguer anuncioAluguer, int Imovel)
        {
            try
            {
                anuncioAluguer.ImovelID = Imovel;
                anuncioAluguer.valido = "false";
                var client = WebApiHttpClient.GetClient();
                string anuncioAluguerJSON = JsonConvert.SerializeObject(anuncioAluguer);
                HttpContent content = new StringContent(anuncioAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio_Aluguer/", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index","Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Aluguer/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioAluguer = JsonConvert.DeserializeObject<Anuncio_Aluguer>(content);
                if (anuncioAluguer == null) return HttpNotFound();
                await PopulateDropDownListImovel(anuncioAluguer.ImovelID);
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
                if (list == null)
                {
                    list = new List<Imovel_Venda_Permuta_Aluguer>();
                }
                ViewBag.Imovel = list;
                return View(anuncioAluguer);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Anuncio_Aluguer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,renda")] Anuncio_Aluguer anuncioAluguer, int Imovel)
        {
            try
            {
                anuncioAluguer.ImovelID = Imovel;
                anuncioAluguer.valido = "false";
                var client = WebApiHttpClient.GetClient();
                string anuncioAluguerJSON = JsonConvert.SerializeObject(anuncioAluguer);
                HttpContent content = new StringContent(anuncioAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Anuncio_Aluguer/" + anuncioAluguer.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Aluguer/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioAluguer = JsonConvert.DeserializeObject<Anuncio_Aluguer>(content);
                if (anuncioAluguer == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioAluguer.ImovelID);
                return View(anuncioAluguer);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Anuncio_Aluguer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Anuncio_Aluguer/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index","Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
        private async Task<Object> PopulateDropDownListImovel(object ImovelID = null)
        {
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            return ViewBag.ImovelID = new SelectList(list, "ID", "ID", ImovelID);

        }
    }
}
