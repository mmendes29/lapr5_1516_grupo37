﻿using ClienteAPP.Helpers;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClienteAPP.Controllers
{
    public class ApplicationUserController : Controller
    {
        // GET: Role
        public async Task<string> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/ApplicationUser/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var id = JsonConvert.DeserializeObject<string>(content);
                return id;
            }
            else
            {
                return null;
            }
        }
    }
}
