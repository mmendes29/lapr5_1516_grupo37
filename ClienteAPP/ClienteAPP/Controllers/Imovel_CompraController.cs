﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAPP.Helpers;
using ClienteAPP.Models;

namespace ClienteAPP.Controllers
{
    public class Imovel_CompraController : Controller
    {

        // GET: Imovel_Compra
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Compra/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelCompra =
                JsonConvert.DeserializeObject<IEnumerable<Imovel_Compra>>(content);
                return View(imovelCompra);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Imovel_Compra/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelCompra = JsonConvert.DeserializeObject<Imovel_Compra>(content);
                if (imovelCompra == null) return HttpNotFound();
                return View(imovelCompra);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Imovel_Compra/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListTipoImovel(0);
            return View();
        }

        // POST: Imovel_Compra/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,precoMin,precoMax,areaMin,areaMax,local,latitude,longitude,TipoImovelID")] Imovel_Compra imovelCompra)
        {
            
            try
            {
                var client = WebApiHttpClient.GetClient();
                string imovelCompraJSON = JsonConvert.SerializeObject(imovelCompra);
                HttpContent content = new StringContent(imovelCompraJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Imovel_Compras", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Compra/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelCompra = JsonConvert.DeserializeObject<Imovel_Compra>(content);
                if (imovelCompra == null) return HttpNotFound();
                return View(imovelCompra);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Imovel_Compra/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,precoMin,precoMax,areaMin,areaMax,local,latitude,longitude,TipoImovelID")] Imovel_Compra imovelCompra)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string imovelCompraJSON = JsonConvert.SerializeObject(imovelCompra);
                HttpContent content = new StringContent(imovelCompraJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Imovel_Compra/" + imovelCompra.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Compra/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Compra/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelCompra = JsonConvert.DeserializeObject<Imovel_Compra>(content);
                if (imovelCompra == null) return HttpNotFound();
                return View(imovelCompra);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Imovel_Compra/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Imovel_Compra/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }
        private async Task<Object> PopulateDropDownListTipoImovel(int? ID = null)
        {
            TipoImovelController ctrl = new TipoImovelController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            TipoImovel TipoImovelID;
            if (ID.Value == 0)
            {
                TipoImovelID = null;
            }
            else
            {
                TipoImovelID = await ctrl.getTipoImovelbyID(ID.Value);
            }

            if (list == null)
            {
                list = new List<TipoImovel>();
            }


            if (TipoImovelID == null)
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID);
            }
            else
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID.ID);
            }

        }
    }
}
