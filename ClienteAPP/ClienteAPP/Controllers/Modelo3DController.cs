﻿using System.Net;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAPP.Helpers;
using ClienteAPP.Models;

namespace ClienteAPP.Controllers
{
    public class Modelo3DController : Controller
    {
        public async Task<ActionResult> Index(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Modelo3D/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var fileToRetrieve = JsonConvert.DeserializeObject<Modelo3D>(content);
                if (fileToRetrieve == null) return HttpNotFound();

                return File(System.Text.Encoding.Default.GetBytes(fileToRetrieve.Content), fileToRetrieve.ContentType);
            }
            else
            {
                return RedirectToAction("Erro", "Home");
            }
        }
    }
}