﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAPP.Helpers;
using ClienteAPP.Models;

namespace ClienteAPP.Controllers
{
    public class Anuncio_PermutaController : Controller
    {

        // GET: Anuncio_Permuta
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Permuta/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosPermuta =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Permuta>>(content);
                List<Imovel_Venda_Permuta_Aluguer> list = new List<Imovel_Venda_Permuta_Aluguer>();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                foreach (Anuncio_Permuta a in anunciosPermuta)
                {
                    list.Add(await ctrl.get(a.ImovelID));
                }
                ViewBag.Imoveis = list;
                return View(anunciosPermuta);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        public async Task<IEnumerable<Anuncio_Permuta>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Permuta/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anunciosPermuta =
                JsonConvert.DeserializeObject<IEnumerable<Anuncio_Permuta>>(content);
                return anunciosPermuta;
            }
            else
            {
                return null;
            }
        }
        // GET: Anuncio_Permuta/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Permuta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioPermuta = JsonConvert.DeserializeObject<Anuncio_Permuta>(content);
                if (anuncioPermuta == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioPermuta.ImovelID);
                return View(anuncioPermuta);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Anuncio_Permuta/Create
        public async Task<ActionResult> Create()
        {
            await PopulateDropDownListImovel(0);
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            ViewBag.Imovel = list;
            return View();
        }
        
        // POST: Anuncio_Permuta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,preco")] Anuncio_Permuta anuncioPermuta, int Imovel)
        {
            try
            {
                anuncioPermuta.ImovelID = Imovel;
                anuncioPermuta.valido = "false";
                var client = WebApiHttpClient.GetClient();
                string anuncioPermutaJSON = JsonConvert.SerializeObject(anuncioPermuta);
                HttpContent content = new StringContent(anuncioPermutaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio_Permuta/", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Permuta/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Permuta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioPermuta = JsonConvert.DeserializeObject<Anuncio_Permuta>(content);
                if (anuncioPermuta == null) return HttpNotFound();

                await PopulateDropDownListImovel(anuncioPermuta.ImovelID);
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
                if (list == null)
                {
                    list = new List<Imovel_Venda_Permuta_Aluguer>();
                }
                ViewBag.Imovel = list;
                return View(anuncioPermuta);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Anuncio_Permuta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,preco")] Anuncio_Permuta anuncioPermuta, int Imovel)
        {
            try
            {
                anuncioPermuta.ImovelID = Imovel;
                anuncioPermuta.valido = "false";
                var client = WebApiHttpClient.GetClient();
                string anuncioPermutaJSON = JsonConvert.SerializeObject(anuncioPermuta);
                HttpContent content = new StringContent(anuncioPermutaJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Anuncio_Permuta/" + anuncioPermuta.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("Erro", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Anuncio_Permuta/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio_Permuta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioPermuta = JsonConvert.DeserializeObject<Anuncio_Permuta>(content);
                if (anuncioPermuta == null) return HttpNotFound();
                Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
                @ViewBag.Imovel = await ctrl.get(anuncioPermuta.ImovelID);
                return View(anuncioPermuta);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Anuncio_Permuta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Anuncio_Permuta/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Anuncios");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListImovel(object ImovelID = null)
        {
            Imovel_Venda_Permuta_AluguerController ctrl = new Imovel_Venda_Permuta_AluguerController();
            IEnumerable<Imovel_Venda_Permuta_Aluguer> list = await ctrl.getAll();
            if (list == null)
            {
                list = new List<Imovel_Venda_Permuta_Aluguer>();
            }
            return ViewBag.ImovelID = new SelectList(list, "ID", "ID", ImovelID);

        }
    }
}
