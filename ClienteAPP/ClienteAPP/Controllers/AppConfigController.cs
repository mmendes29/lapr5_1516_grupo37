﻿using ClienteAPP.Helpers;
using ClienteAPP.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ClienteAPP.Controllers
{
    public class AppConfigController : Controller
    {
        // GET: AppConfig
        public async Task<ActionResult> Index()
        {
            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AppConfig/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var appConfig = JsonConvert.DeserializeObject<IEnumerable<AppConfig>>(content);
                ViewBag.AppConfig = appConfig;

                return View();
            }
            else
                return RedirectToAction("UnAuthenticated", "Home");
        }

        // GET: AppConfig/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AppConfig/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AppConfigId, IntervaloAlertas, IntervaloCorresp")] AppConfig appConfig, HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    appConfig.Content = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));

                appConfig.ContentType = upload.ContentType;
            }

            try
            {
                var client = WebApiHttpClient.GetClient();
                string AppConfigJSON = JsonConvert.SerializeObject(appConfig);
                HttpContent content = new StringContent(AppConfigJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/AppConfig/", content);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index", "AppConfig");

                else
                    return RedirectToAction("Erro", "Home");
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: AppConfig/Edit/1
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AppConfig/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var appConfig = JsonConvert.DeserializeObject<AppConfig>(content);
                if (appConfig == null)
                    return HttpNotFound();

                appConfig.Content = "";
                return View(appConfig);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: AppConfig/Edit/1
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AppConfigId, IntervaloAlertas, IntervaloCorresp")] AppConfig appConfig, HttpPostedFileBase upload)
        {
            try
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        appConfig.Content = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));

                    appConfig.ContentType = upload.ContentType;
                }

                var client = WebApiHttpClient.GetClient();
                string appConfigJSON = JsonConvert.SerializeObject(appConfig);
                HttpContent content = new StringContent(appConfigJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/AppConfig/" + appConfig.AppConfigId, content);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("UnAuthenticated", "Home");
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }

        }
    }
}
