﻿using ClienteAPP.Helpers;
using ClienteAPP.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace ClienteAPP.Controllers
{
    public class AnuncioAlertaController : Controller
    {
        // GET: AnuncioAlerta
        public async Task<ActionResult> Index()
        {
            ApplicationUserController userCtrl = new ApplicationUserController();
            string userId = await userCtrl.Index();

            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            ViewBag.User = await userCtrl.Index();
            AlertasController alertaCtrl = new AlertasController();
            IEnumerable<AnuncioAlerta> anunciosAlertaEnum = await getAll();
            if (anunciosAlertaEnum != null)
            {
                Anuncio_AluguerController anuncioAluguerCtrl = new Anuncio_AluguerController();
                IEnumerable<Anuncio_Aluguer> anuncioAluguerEnum = await anuncioAluguerCtrl.getAll();

                Anuncio_CompraController anuncioCompraCtrl = new Anuncio_CompraController();
                IEnumerable<Anuncio_Compra> anuncioCompraEnum = await anuncioCompraCtrl.getAll();

                Anuncio_PermutaController anuncioPermutaCtrl = new Anuncio_PermutaController();
                IEnumerable<Anuncio_Permuta> anuncioPermutaEnum = await anuncioPermutaCtrl.getAll();

                Anuncio_VendaController anuncioVendaCtrl = new Anuncio_VendaController();
                IEnumerable<Anuncio_Venda> anuncioVendaEnum = await anuncioVendaCtrl.getAll();

                Imovel_Venda_Permuta_AluguerController ImovelCtrl = new Imovel_Venda_Permuta_AluguerController();
                List<Imovel_Venda_Permuta_Aluguer> ImovelAluguerList = new List<Imovel_Venda_Permuta_Aluguer>();
                List<Imovel_Venda_Permuta_Aluguer> ImovelPermutaList = new List<Imovel_Venda_Permuta_Aluguer>();
                List<Imovel_Venda_Permuta_Aluguer> ImovelVendaList = new List<Imovel_Venda_Permuta_Aluguer>();

                List<Anuncio_Aluguer> anuncioAluguerList = new List<Anuncio_Aluguer>();
                List<Anuncio_Compra> anuncioCompraList = new List<Anuncio_Compra>();
                List<Anuncio_Permuta> anuncioPermutaList = new List<Anuncio_Permuta>();
                List<Anuncio_Venda> anuncioVendaList = new List<Anuncio_Venda>();

                foreach (var anuncioAlerta in anunciosAlertaEnum)
                {
                    Alerta alerta = await alertaCtrl.get(anuncioAlerta.AlertaId);
                    if (alerta != null && alerta.applicationUserID == userId)
                    {
                        foreach (Anuncio_Aluguer anuncioAluguer in anuncioAluguerEnum)
                        {
                            if(anuncioAlerta.AnuncioId == anuncioAluguer.ID)
                            {
                                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncioAluguer.ImovelID);
                                anuncioAluguerList.Add(anuncioAluguer);
                                if (imovel != null)
                                    ImovelAluguerList.Add(imovel);
                            }
                        }

                        foreach (Anuncio_Compra anuncioCompra in anuncioCompraEnum)
                            if (anuncioAlerta.AnuncioId == anuncioCompra.ID)
                                anuncioCompraList.Add(anuncioCompra);

                        foreach (Anuncio_Permuta anuncioPermuta in anuncioPermutaEnum)
                        {
                            if (anuncioAlerta.AnuncioId == anuncioPermuta.ID)
                            {
                                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncioPermuta.ImovelID);
                                anuncioPermutaList.Add(anuncioPermuta);
                                if (imovel != null)
                                    ImovelPermutaList.Add(imovel);
                            }
                        }

                        foreach (Anuncio_Venda anuncioVenda in anuncioVendaEnum)
                        {
                            if (anuncioAlerta.AnuncioId == anuncioVenda.ID)
                            {
                                Imovel_Venda_Permuta_Aluguer imovel = await ImovelCtrl.get(anuncioVenda.ImovelID);
                                anuncioVendaList.Add(anuncioVenda);
                                if (imovel != null)
                                    ImovelVendaList.Add(imovel);
                            }
                        }
                    }
                }
                ViewBag.ImovelAluguer = ImovelAluguerList;
                ViewBag.ImovelPermuta = ImovelPermutaList;
                ViewBag.ImovelVenda = ImovelVendaList;

                ViewBag.AnuncioAluguer = anuncioAluguerList;
                ViewBag.AnuncioCompra = anuncioCompraList;
                ViewBag.AnuncioPermuta = anuncioPermutaList;
                ViewBag.AnuncioVenda = anuncioVendaList;
            }
            return View();
        }

        public async Task<IEnumerable<AnuncioAlerta>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AnuncioAlerta/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncioAlerta = JsonConvert.DeserializeObject<IEnumerable<AnuncioAlerta>>(content);
                return anuncioAlerta;
            }
            else
            {
                return null;
            }
        }
    }
}