﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using ClienteAPP.Models;

namespace ClienteAPP.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            RoleController role = new RoleController();
            List<string> l = await role.Index();
            if (l == null)
            {
                return View("UnAuthenticated");
            }

            if (l.Contains("Admin"))
            {
                return View("Admin");
            }

            else
            {
                int count = 0;
                ApplicationUserController userCtrl = new ApplicationUserController();
                string userId = await userCtrl.Index();

                AlertasController alertaCtrl = new AlertasController();
                AnuncioAlertaController anuncioAlertaCtrl = new AnuncioAlertaController();
                IEnumerable<AnuncioAlerta> anunciosAlertaEnum = await anuncioAlertaCtrl.getAll();
                if (anunciosAlertaEnum != null)
                {
                    foreach (var anuncioAlerta in anunciosAlertaEnum)
                    {
                        Alerta alerta = await alertaCtrl.get(anuncioAlerta.AlertaId);
                        if (alerta != null && alerta.applicationUserID == userId)
                            count++;
                    }
                }
                ViewBag.alertas = count;
                return View();
            }
        }

        public ActionResult UnAuthenticated()
        {
            return View();
        }

        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult ErroLogin()
        {
            return View();
        }

        public ActionResult Erro()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Descrição da aplicação.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Projeto realizado por:";

            return View();
        }
    }
}