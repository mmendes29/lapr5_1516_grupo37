﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAPP.Helpers;
using ClienteAPP.Models;
using System.Linq;

namespace ClienteAPP.Controllers
{
    public class Imovel_Venda_Permuta_AluguerController : Controller
    {

        // GET: Imovel_Venda_Permuta_Aluguer
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Imovel_Venda_Permuta_Aluguer>>(content);
                return View(imovelVendaPermutaAluguer);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }


        public async Task<IEnumerable<Imovel_Venda_Permuta_Aluguer>> getAll()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer =
                JsonConvert.DeserializeObject<IEnumerable<Imovel_Venda_Permuta_Aluguer>>(content);
                return imovelVendaPermutaAluguer;
            }
            else
            {
                return null;
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Details/5
        public async Task<Imovel_Venda_Permuta_Aluguer> get(int? id)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);

                return imovelVendaPermutaAluguer;
            }
            else
            {
                return null;
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();
                return View(imovelVendaPermutaAluguer);
            }
            else
            {
                return RedirectToAction("UnAuthenticated", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Create
        public async Task<ActionResult> Create()
        {
            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AppConfig/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var appConfig = JsonConvert.DeserializeObject<IEnumerable<AppConfig>>(content);

                string tmp = "";
                foreach (var app in appConfig)
                    tmp = app.Content.ToString();

                List<string> local = tmp.Split('\n').ToList();
                ViewBag.local = new SelectList(local);
                await PopulateDropDownListTipoImovel(0);
                List<string> DiasDaSemana = new List<string> { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                ViewBag.DiasDaSemana = DiasDaSemana;

                return View();
            }
            else
                return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID, TipoImovelID, area, local, latitude, longitude, pontoInteresse, HoraAbertura, HoraFecho, DuraçãoVisita")] Imovel_Venda_Permuta_Aluguer imovelVendaPermutaAluguer, string[] DiasDaSemana, HttpPostedFileBase upload, HttpPostedFileBase upload3D)
        {
            try
            {
                if (DiasDaSemana != null && DiasDaSemana.Length != 0)
                {
                    string tmp = "";
                    foreach (string dia in DiasDaSemana)
                        tmp += dia + ";";

                    imovelVendaPermutaAluguer.DiasSemana = tmp;
                }

                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imovelVendaPermutaAluguer.fotoContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));
                    }
                    
                    imovelVendaPermutaAluguer.fotoContentType = upload.ContentType;
                }

                if (upload3D != null && upload3D.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload3D.InputStream))
                    {
                        imovelVendaPermutaAluguer.modelo3DContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload3D.ContentLength));
                    }

                    imovelVendaPermutaAluguer.modelo3DContentType = upload3D.ContentType;
                }

                var client = WebApiHttpClient.GetClient();
                string imovelVendaPermutaAluguerJSON = JsonConvert.SerializeObject(imovelVendaPermutaAluguer);
                HttpContent content = new StringContent(imovelVendaPermutaAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Imovel_Venda_Permuta_Aluguer", content);
                 if (response.IsSuccessStatusCode)
                  {
                      return RedirectToAction("Index");
                  }
                  else
                  {
                return RedirectToAction("UnAuthenticated", "Home");
                  }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (WebApiHttpClient.getToken() == null)
                return RedirectToAction("UnAuthenticated", "Home");

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();

                await PopulateDropDownListTipoImovel(imovelVendaPermutaAluguer.tipoImovelID);
                imovelVendaPermutaAluguer.fotoContent = "";
                List<string> DiasDaSemana = new List<string> { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                ViewBag.DiasDaSemana = DiasDaSemana;

                response = await client.GetAsync("api/AppConfig/");
                if (response.IsSuccessStatusCode)
                {
                    content = await response.Content.ReadAsStringAsync();
                    var appConfig = JsonConvert.DeserializeObject<IEnumerable<AppConfig>>(content);

                    string tmp = "";
                    foreach (var app in appConfig)
                        tmp = app.Content.ToString();

                    List<string> local = tmp.Split('\n').ToList();
                    ViewBag.local = new SelectList(local);

                    return View(imovelVendaPermutaAluguer);
                }
                else
                    return RedirectToAction("UnAuthenticated", "Home");
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID, applicationUserID, TipoImovelID, area, local, latitude, longitude, pontoInteresse, HoraAbertura, HoraFecho, DuraçãoVisita")] Imovel_Venda_Permuta_Aluguer imovelVendaPermutaAluguer, string[] DiasDaSemana, HttpPostedFileBase upload, HttpPostedFileBase upload3D)
        {
            try
            {

                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        imovelVendaPermutaAluguer.fotoContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload.ContentLength));
                    }

                    imovelVendaPermutaAluguer.fotoContentType = upload.ContentType;
                }

                if (upload3D != null && upload3D.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload3D.InputStream))
                    {
                        imovelVendaPermutaAluguer.modelo3DContent = System.Text.Encoding.Default.GetString(reader.ReadBytes(upload3D.ContentLength));
                    }

                    imovelVendaPermutaAluguer.modelo3DContentType = upload3D.ContentType;
                }

                if (DiasDaSemana != null && DiasDaSemana.Length != 0)
                {
                    string tmp = "";
                    foreach (string dia in DiasDaSemana)
                        tmp += dia + ";";

                    imovelVendaPermutaAluguer.DiasSemana = tmp;
                }

                var client = WebApiHttpClient.GetClient();
                string imovelVendaPermutaAluguerJSON = JsonConvert.SerializeObject(imovelVendaPermutaAluguer);
                HttpContent content = new StringContent(imovelVendaPermutaAluguerJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Imovel_Venda_Permuta_Aluguer/" + imovelVendaPermutaAluguer.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        // GET: Imovel_Venda_Permuta_Aluguer/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovelVendaPermutaAluguer = JsonConvert.DeserializeObject<Imovel_Venda_Permuta_Aluguer>(content);
                if (imovelVendaPermutaAluguer == null) return HttpNotFound();
                return View(imovelVendaPermutaAluguer);
            }
            return RedirectToAction("UnAuthenticated", "Home");
        }

        // POST: Imovel_Venda_Permuta_Aluguer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Imovel_Venda_Permuta_Aluguer/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("UnAuthenticated", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Erro", "Home");
            }
        }

        private async Task<Object> PopulateDropDownListTipoImovel(int? ID = null)
        {
            TipoImovelController ctrl = new TipoImovelController();
            IEnumerable<TipoImovel> list = await ctrl.getAll();
            TipoImovel TipoImovelID;
            if(ID.Value==0)
            {
                TipoImovelID = null;
            } else
            {
                 TipoImovelID = await ctrl.getTipoImovelbyID(ID.Value);
            }

            if (list == null)
            {
                list = new List<TipoImovel>();
            }

            if (TipoImovelID == null)
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID);
            }
            else
            {
                return ViewBag.TipoImovelID = new SelectList(list, "ID", "nome", TipoImovelID.ID);
            }

        }
    }
}
