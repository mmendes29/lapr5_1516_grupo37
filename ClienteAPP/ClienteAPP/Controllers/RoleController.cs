﻿using ClienteAPP.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ClienteAPP.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public async Task<List<string>> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles = JsonConvert.DeserializeObject<IEnumerable<string>>(content);
                return roles.ToList();
            }
            else
                return null;
        }

        public async Task<List<string>> GetUserRoles(string UserId)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role/GetUserRoles/" + UserId);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles = JsonConvert.DeserializeObject<IEnumerable<string>>(content);
                return roles.ToList();
            }
            else
                return null;
        }

        public async Task<ActionResult> PromoverUser(string UserId)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role/PromoverUser/" + UserId);
            if (response.IsSuccessStatusCode)
                return RedirectToAction("Index", "Users");
            else
                return RedirectToAction("Erro", "Home");
        }

        public async Task<ActionResult> DespromoverUser(string UserId)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role/DespromoverUser/" + UserId);
            if (response.IsSuccessStatusCode)
                return RedirectToAction("Index", "Users");
            else
                return RedirectToAction("Erro", "Home");
        }
    }
}

