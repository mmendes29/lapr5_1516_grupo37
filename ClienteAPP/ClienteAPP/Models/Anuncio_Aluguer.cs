﻿using System.ComponentModel;

namespace ClienteAPP.Models
{
    public class Anuncio_Aluguer
    {

        public int ID { get; set; }
        public int ImovelID { get; set; }
        public string applicationUserID { get; set; }
        
        
        [DisplayName("Renda (€)")]
        public float renda { get; set; }

        public int tipoImovelID { get; set; }
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }
        public string local { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public float area { get; set; }
        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }
        public string valido { get; set; }
        public string MediadorID { get; set; }
    }
}