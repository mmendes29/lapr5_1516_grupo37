﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClienteAPP.Models
{
    public class AnuncioAlerta
    {
        public int AnuncioAlertaId { get; set; }
        public int AlertaId { get; set; }
        public int AnuncioId { get; set; }
    }
}
