﻿using System.ComponentModel;

namespace ClienteAPP.Models
{
    public class TipoImovel
    {
        public int ID { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public string nome { get; set; }
        public int subtipo_id { get; set; }

        [DisplayName("Subtipo")]
        public string subtipo { get; set; }
    }
}