﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ClienteAPP.Models
{
    public class Imovel_Venda_Permuta_Aluguer
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }

        [DisplayName("Tipo de Imovel")]
        public string tipoImovel { get; set; }
        public int localizacaoID { get; set; }

        [DisplayName("Local")]
        public string local { get; set; }

        [Range(-90, 90, ErrorMessage = "Insira um valor válido (-90 a 90 graus)"), DisplayName("Latitude")]
        public string latitude { get; set; }

        [Range(-180, 180, ErrorMessage = "Insira um valor válido (-180 a 180 graus)"), DisplayName("Longitude")]
        public string longitude { get; set; }

        [DisplayName("Área")]
        public float area { get; set; }

        public int fotoID { get; set; }
        public string fotoContent { get; set; }
        public string fotoContentType { get; set; }

        public int modelo3DID { get; set; }
        public string modelo3DContent { get; set; }
        public string modelo3DContentType { get; set; }

        public int PontoInteresseId { get; set; }

        [DisplayName("Pontos de interesse")]
        public string pontoInteresse { get; set; }

        public int HorarioVisitaID { get; set; }

        [Range(8, 19, ErrorMessage = "Insira um valor válido (8 a 19)"), DisplayName("Hora de abertura")]
        public int HoraAbertura { get; set; }

        [Range(9, 20, ErrorMessage = "Insira um valor válido (9 a 20)"), DisplayName("Hora de fecho")]
        public int HoraFecho { get; set; }

        [DisplayName("Dias da semana")]
        public string DiasSemana { get; set; }

        [DisplayName("Duração da visita")]
        public int DuraçãoVisita { get; set; }
    }
}
