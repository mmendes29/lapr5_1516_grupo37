﻿using System.ComponentModel;

namespace ClienteAPP.Models
{
    public class Imovel_Compra
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }
        public int tipoImovelID { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public string tipoImovel { get; set; }

        public int localizacaoID { get; set; }

        [DisplayName("Local")]
        public string local { get; set; }

        [DisplayName("Latitude")]
        public string latitude { get; set; }

        [DisplayName("Longitude")]
        public string longitude { get; set; }

        [DisplayName("Área Mínimo")]
        public float areaMin { get; set; }

        [DisplayName("Área Máximo")]
        public float areaMax { get; set; }
    }
}