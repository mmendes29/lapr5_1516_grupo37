﻿using System.ComponentModel;

namespace ClienteAPP.Models
{
    public class AppConfig
    {
        public int AppConfigId { get; set; }

        [DisplayName("Intervalo Alertas (min)")]
        public int IntervaloAlertas { get; set; }

        [DisplayName("Intervalo Correspondência (min)")]
        public int IntervaloCorresp { get; set; }

        [DisplayName("Ficheiro")]
        public string Content { get; set; }

        public string ContentType { get; set; }
    }
}