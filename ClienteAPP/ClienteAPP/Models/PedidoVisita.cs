﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClienteAPP.Models
{
    public class PedidoVisita
    {
        [HiddenInput(DisplayValue = false)]
        public int PedidoVisitaId { get; set; }

        [Required, HiddenInput(DisplayValue = false), DisplayName("ID do cliente")]
        public string ClienteId { get; set; }

        [Required, HiddenInput(DisplayValue = false), DisplayName("ID do mediador")]
        public string MediadorId { get; set; }

        [Required, HiddenInput(DisplayValue = false), DisplayName("ID do imóvel")]
        public int ImovelId { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Data { get; set; }

        [Required, DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm}")]
        public DateTime Hora { get; set; }

        [DisplayName("Estado do pedido de visita")]
        public string Estado { get; set; }
    }
}
