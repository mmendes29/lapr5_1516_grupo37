﻿using System.ComponentModel.DataAnnotations;

namespace ClienteAPP.Models
{
    public class Foto
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
    }
}