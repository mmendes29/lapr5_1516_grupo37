﻿using System.ComponentModel.DataAnnotations;

namespace ClienteAPP.Models
{
    public class Modelo3D
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
    }
}