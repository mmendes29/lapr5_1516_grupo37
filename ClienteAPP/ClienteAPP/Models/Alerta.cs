﻿using System;
using System.Collections.Generic;

namespace ClienteAPP.Models
{
    public class Alerta
    {
        public int ID { get; set; }
        public string applicationUserID { get; set; }

        public List<List<String>> fixo { get; set; }
        public List<List<String>> parametroAlfaNumerico { get; set; }
        public List<List<String>> parametroIntervaloNumerico { get; set; }
    }
}
