﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Modelo3DController : ApiController
    {
        private IRepository<Modelo3DDTO> repo = new Modelo3DRepository();

        // GET: api/Modelos3D
        public IEnumerable<Modelo3DDTO> GetModelos3D()
        {
            return repo.GetData().ToList();
        }

        // GET: api/Modelos3D/5
        [ResponseType(typeof(Modelo3DDTO))]
        public IHttpActionResult GetModelo3D(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/Modelos3D/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutModelo3D(int id, Modelo3DDTO modelo)
        {
            repo.Update(id, modelo);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Modelos3D
        [ResponseType(typeof(Modelo3DDTO))]
        public IHttpActionResult PostModelo3D(Modelo3DDTO modelo3D)
        {
            repo.Create(modelo3D);

            return CreatedAtRoute("DefaultApi", new { id = modelo3D.ID }, modelo3D);
        }

        // DELETE: api/Modelos3D/5
        [ResponseType(typeof(Modelo3D))]
        public IHttpActionResult DeleteModelo3D(int id)
        {
            repo.Delete(id);

            return Ok();
        }
    }
}