﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class AnuncioAlertaController : ApiController
    {
        private IRepository<AnuncioAlertaDTO> repo = new AnuncioAlertaRepository();

        // GET: api/AnuncioAlerta
        public IEnumerable<AnuncioAlertaDTO> GetAnuncioAlerta()
        {
            return repo.GetData().ToList();
        }

        // GET: api/AnuncioAlerta/5
        [ResponseType(typeof(AnuncioAlertaDTO))]
        public IHttpActionResult GetAnuncioAlerta(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/AnuncioAlerta/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnuncioAlerta(int id, AnuncioAlertaDTO anuncioAlerta)
        {
            repo.Update(id, anuncioAlerta);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AnuncioAlerta
        [ResponseType(typeof(AnuncioAlertaDTO))]
        public IHttpActionResult PostAnuncioAlerta(AnuncioAlertaDTO anuncioAlerta)
        {
            repo.Create(anuncioAlerta);

            return CreatedAtRoute("DefaultApi", new { id = anuncioAlerta.AnuncioAlertaId }, anuncioAlerta);
        }

        // DELETE: api/AnuncioAlerta/5
        [ResponseType(typeof(AnuncioAlerta))]
        public IHttpActionResult DeleteAnuncioAlerta(int id)
        {
            repo.Delete(id);

            return Ok();
        }
    }
}
