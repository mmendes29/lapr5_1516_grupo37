﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class HorariosVisitaController : ApiController
    {
        private IRepository<HorarioVisitaDTO> repo = new HorarioVisitaRepository();

        // GET: api/HorariosVisita
        public IEnumerable<HorarioVisitaDTO> GetHorariosVisita()
        {
            return repo.GetData();
        }

        // GET: api/HorariosVisita/5
        [ResponseType(typeof(HorarioVisita))]
        public IHttpActionResult GetHorariosVisita(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/HorariosVisita/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHorariosVisita(int id, HorarioVisitaDTO horario)
        {
            repo.Update(id, horario);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HorariosVisita
        [ResponseType(typeof(HorarioVisita))]
        public IHttpActionResult PostHorarioVisita(HorarioVisitaDTO horario)
        {
            repo.Create(horario);
            return CreatedAtRoute("DefaultApi", new { id = horario.HorarioVisitaId }, horario);
        }

        // DELETE: api/HorariosVisita/5
        [ResponseType(typeof(HorarioVisita))]
        public IHttpActionResult DeleteHorarioVisita(int id)
        {
            repo.Delete(id);
            return Ok();
        }
    }
}