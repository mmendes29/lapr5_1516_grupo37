﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using ClassLibrary.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class AppConfigController : ApiController
    {
        private IRepository<AppConfigDTO> repo = new AppConfigRepository();

        // GET: AppConfig
        public IEnumerable<AppConfigDTO> GetAppConfigs()
        {
            return repo.GetData().ToList();
        }

        // GET: api/AppConfig/5
        [ResponseType(typeof(AppConfigDTO))]
        public IHttpActionResult GetAppConfig(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/AppConfig/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAppConfig(int id, AppConfigDTO cfg)
        {
            repo.Update(id, cfg);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AppConfig
        [ResponseType(typeof(AppConfigDTO))]
        public IHttpActionResult PostAppConfig(AppConfigDTO cfg)
        {
            repo.Create(cfg);
            return CreatedAtRoute("DefaultApi", new { id = cfg.AppConfigId }, cfg);
        }

        // DELETE: api/AppConfig/5
        [ResponseType(typeof(AppConfig))]
        public IHttpActionResult DeleteAppConfig(int id)
        {
            repo.Delete(id);
            return Ok();
        }
    }
}
