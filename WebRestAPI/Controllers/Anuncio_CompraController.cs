﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ClassLibrary.DTO;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Anuncio_CompraController : ApiController
    {
        //private IAnuncio_CompraRepository repo = new Anuncio_CompraRepository();

        private IRepository<Anuncio_CompraDTO> repo = new Anuncio_CompraRepository();

        // GET: api/Anuncio_Compra
        [AllowAnonymous]
        public IEnumerable<Anuncio_CompraDTO> GetAnuncios()
        {
            return repo.GetData();
        }

        // GET: api/Anuncio_Compra/5
        [ResponseType(typeof(Anuncio_CompraDTO))]
        public IHttpActionResult GetAnuncio_Compra(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/Anuncio_Compra/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnuncio_Compra(int id, Anuncio_CompraDTO anuncio_Compra)
        {
            Anuncio_CompraDTO a = repo.GetData(id);

            if (User.Identity.GetUserId() != a.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            if (anuncio_Compra.valido == "true")
            {
                anuncio_Compra.MediadorID = User.Identity.GetUserId();
            }
            repo.Update(id,anuncio_Compra);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anuncio_Compra
        [ResponseType(typeof(Anuncio_CompraDTO))]
        public IHttpActionResult PostAnuncio_Compra(Anuncio_CompraDTO anuncio_Compra)
        {
            anuncio_Compra.applicationUserID = User.Identity.GetUserId();
            repo.Create(anuncio_Compra);
            return CreatedAtRoute("DefaultApi", new { id = anuncio_Compra.ID }, anuncio_Compra);
        }

        // DELETE: api/Anuncio_Compra/5
        [ResponseType(typeof(Anuncio_CompraDTO))]
        public IHttpActionResult DeleteAnuncio_Compra(int id)
        {
            Anuncio_CompraDTO anuncio_Compra = repo.GetData(id);
            if (User.Identity.GetUserId() != anuncio_Compra.applicationUserID && !User.IsInRole("Mediador"))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            repo.Delete(id);

            return Ok();
        }

    }
}