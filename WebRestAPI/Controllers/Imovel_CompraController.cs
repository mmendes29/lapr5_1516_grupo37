﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Imovel_CompraController : ApiController
    {
        private IRepository<Imovel_CompraDTO> repo = new Imovel_CompraRepository();

        // GET: api/Imovel_Compra
        public IEnumerable<Imovel_CompraDTO> GetImovels()
        {
            return repo.GetData().ToList();
        }

        // GET: api/Imovel_Compra/5
        [ResponseType(typeof(Imovel_CompraDTO))]
        public IHttpActionResult GetImovel_Compra(int id)
        {
          
            return Ok(repo.GetData(id));
        }

        // PUT: api/Imovel_Compra/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImovel_Compra(int id, Imovel_CompraDTO imovel_Compra)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //if (id != imovel_Compra.ID)
            //{
            //    return BadRequest();
            //}

            //db.Entry(imovel_Compra).State = EntityState.Modified;

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!Imovel_CompraExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            repo.Update(id, imovel_Compra);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Imovel_Compra
        [ResponseType(typeof(Imovel_CompraDTO))]
        public IHttpActionResult PostImovel_Compra(Imovel_CompraDTO imovel_Compra)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //db.Imovel_Compra.Add(imovel_Compra);
            //db.SaveChanges();
            repo.Create(imovel_Compra);
            return CreatedAtRoute("DefaultApi", new { id = imovel_Compra.ID }, imovel_Compra);
        }

        // DELETE: api/Imovel_Compra/5
        [ResponseType(typeof(Imovel_CompraDTO))]
        public IHttpActionResult DeleteImovel_Compra(int id)
        {
            repo.Delete(id);
            //if (imovel_Compra == null)
            //{
            //    return NotFound();
            //}

            //db.Imovel_Compra.Remove(imovel_Compra);
            //db.SaveChanges();

            return Ok();
        }
    }
}