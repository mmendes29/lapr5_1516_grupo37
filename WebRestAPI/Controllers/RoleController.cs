﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Security;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class RoleController : ApiController
    {
        // GET api/<controller>
        public List<string> Get()
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            string id = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(id);
            List<IdentityUserRole> l =  user.Roles.ToList();
            List<string> list = new List<string>();
            for (int i = 0; i < l.Count; i++)
            {
                list.Add(roleManager.FindById(l[i].RoleId).Name);
            }
                return list;
        }


        // GET api/Role/GetUserRoles/UserId
        [Route("api/Role/GetUserRoles/{UserId}")]
        public List<string> GetUserRoles(string UserId)
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            ApplicationUser user = userManager.FindById(UserId);
            List<IdentityUserRole> l = user.Roles.ToList();
            List<string> list = new List<string>();
            for (int i = 0; i < l.Count; i++)
            {
                list.Add(roleManager.FindById(l[i].RoleId).Name);
            }
            return list;
        }

        // api/Role/PromoverUser/UserId
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("api/Role/PromoverUser/{UserId}")]
        public IHttpActionResult PromoverUser(string UserId)
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            userManager.RemoveFromRole(UserId, "Cliente");
            userManager.AddToRole(UserId, "Mediador");

            return Ok();
        }

        // api/Role/DespromoverUser/UserId
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("api/Role/DespromoverUser/{UserId}")]
        public IHttpActionResult DespromoverUser(string UserId)
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            userManager.RemoveFromRole(UserId, "Mediador");
            userManager.AddToRole(UserId, "Cliente");

            return Ok();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
