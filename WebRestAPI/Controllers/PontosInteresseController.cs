﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class PontosInteresseController : ApiController
    {
        private IRepository<PontoInteresseDTO> repo = new PontoInteresseRepository();

        // GET: api/PontosInteresse
        public IEnumerable<PontoInteresseDTO> GetPontosInteresse()
        {
            return repo.GetData().ToList();
        }

        // GET: api/PontosInteresse/5
        [ResponseType(typeof(PontoInteresseDTO))]
        public IHttpActionResult GetPontoInteresse(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/PontosInteresse/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPontoInteresse(int id, PontoInteresseDTO pontoInteresse)
        {
            repo.Update(id, pontoInteresse);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PontosInteresse
        [ResponseType(typeof(PontoInteresseDTO))]
        public IHttpActionResult PostPontoInteresse(PontoInteresseDTO pontoInteresse)
        {
            repo.Create(pontoInteresse);

            return CreatedAtRoute("DefaultApi", new { id = pontoInteresse.PontoInteresseId }, pontoInteresse);
        }

        // DELETE: api/PontosInteresse/5
        [ResponseType(typeof(PontoInteresse))]
        public IHttpActionResult DeletePontoInteresse(int id)
        {
            repo.Delete(id);

            return Ok();
        }
    }
}