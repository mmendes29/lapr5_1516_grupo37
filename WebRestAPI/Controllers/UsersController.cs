﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
        // GET api/<controller>
        public List<string> Get()
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            List<ApplicationUser> list = context.Users.ToList();
            List<string> users = new List<string>();

            for (int i = 0; i < list.Count; i++)
            {
                users.Add(userManager.FindById(list[i].Id).Id);
            }
            return users;
        }

        // GET api/<controller>/5
        public string Get(string id)
        {
            var context = new ImoveisDbContext();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            string userEmail = null;
            List<ApplicationUser> list = context.Users.ToList();

            foreach (ApplicationUser user in list)
                if (user.Id == id)
                    userEmail = user.Email;

            return userEmail;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
