﻿using ClassLibrary.DAL;
using ClassLibrary.DTO;
using ClassLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class PedidoVisitaController : ApiController
    {
        private IRepository<PedidoVisitaDTO> repo = new PedidoVisitasRepository();

        // GET: api/PedidoVisita
        public IEnumerable<PedidoVisitaDTO> GetPedidoVisita()
        {
            List<PedidoVisitaDTO> ret = new List<PedidoVisitaDTO>();
            foreach (PedidoVisitaDTO visita in repo.GetData())
            {
                if (visita.ClienteId.Equals(User.Identity.GetUserId()))
                    ret.Add(visita);
            }
            return ret;
        }

        // GET: api/PedidoVisitaMediador
        [HttpGet]
        [Route("api/PedidoVisita/PedidoVisitaMediador/")]
        public IEnumerable<PedidoVisitaDTO> GetPedidoVisitaMediador()
        {
            List<PedidoVisitaDTO> ret = new List<PedidoVisitaDTO>();
            foreach (PedidoVisitaDTO visita in repo.GetData())
                if (visita.MediadorId.Equals(User.Identity.GetUserId()))
                    ret.Add(visita);

            return ret;
        }

        // GET: api/PedidoVisita/5
        [ResponseType(typeof(PedidoVisita))]
        public IHttpActionResult GetPedidoVisita(int id)
        {
            PedidoVisitaDTO visita = repo.GetData(id);
            if (!visita.ClienteId.Equals(User.Identity.GetUserId()))
                return Ok();

            return Ok(repo.GetData(id));
        }

        // PUT: api/PedidoVisita/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPedidoVisita(int id, PedidoVisitaDTO visita)
        {
            PedidoVisitaDTO a = repo.GetData(id);
            if (!visita.ClienteId.Equals(User.Identity.GetUserId()))
                return StatusCode(HttpStatusCode.Forbidden);

            repo.Update(id, visita);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PedidoVisita
        [ResponseType(typeof(PedidoVisitaDTO))]
        public IHttpActionResult PostPedidoVisita(PedidoVisitaDTO visita)
        {
            visita.ClienteId = User.Identity.GetUserId();
            repo.Create(visita);
            return CreatedAtRoute("DefaultApi", new { id = visita.PedidoVisitaId }, visita);
        }

        // DELETE: api/PedidoVisita/5
        [ResponseType(typeof(PedidoVisitaDTO))]
        public IHttpActionResult DeletePedidoVisita(int id)
        {
            PedidoVisitaDTO visita = repo.GetData(id);
            if (!visita.ClienteId.Equals(User.Identity.GetUserId()))
                return StatusCode(HttpStatusCode.Forbidden);

            repo.Delete(id);
            return Ok();
        }
    }
}
