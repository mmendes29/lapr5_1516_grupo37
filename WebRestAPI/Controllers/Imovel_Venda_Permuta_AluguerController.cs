﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Models;
using ClassLibrary.DTO;
using Microsoft.AspNet.Identity;

namespace WebRestAPI.Controllers
{
    [Authorize]
    public class Imovel_Venda_Permuta_AluguerController : ApiController
    {
        private IRepository<Imovel_Venda_Permuta_AluguerDTO> repo = new Imovel_Venda_Permuta_AluguerRepository();

        // GET: api/Imovel_Venda_Permuta_Aluguer
        public IEnumerable<Imovel_Venda_Permuta_AluguerDTO> GetImovels()
        {
            List<Imovel_Venda_Permuta_AluguerDTO> ret = new List<Imovel_Venda_Permuta_AluguerDTO>();
            foreach(Imovel_Venda_Permuta_AluguerDTO imovel in repo.GetData())
            {
                if(imovel.applicationUserID==User.Identity.GetUserId())
                {
                    ret.Add(imovel);
                }
            }
            return ret;
        }

        // GET: api/Imovel_Venda_Permuta_Aluguer/5
        [AllowAnonymous]
        [ResponseType(typeof(Imovel_Venda_Permuta_AluguerDTO))]
        public IHttpActionResult GetImovel_Venda_Permuta_Aluguer(int id)
        {
            return Ok(repo.GetData(id));
        }

        // PUT: api/Imovel_Venda_Permuta_Aluguer/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImovel_Venda_Permuta_Aluguer(int id, Imovel_Venda_Permuta_AluguerDTO imovel_Venda_Permuta_Aluguer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imovel_Venda_Permuta_Aluguer.ID)
            {
                return BadRequest();
            }

            if (User.Identity.GetUserId() != imovel_Venda_Permuta_Aluguer.applicationUserID)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            repo.Update(id, imovel_Venda_Permuta_Aluguer);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Imovel_Venda_Permuta_Aluguer
        [ResponseType(typeof(Imovel_Venda_Permuta_AluguerDTO))]
        public IHttpActionResult PostImovel_Venda_Permuta_Aluguer(Imovel_Venda_Permuta_AluguerDTO imovel_Venda_Permuta_Aluguer)
        {
            imovel_Venda_Permuta_Aluguer.applicationUserID = User.Identity.GetUserId();
            repo.Create(imovel_Venda_Permuta_Aluguer);

            return CreatedAtRoute("DefaultApi", new { id = imovel_Venda_Permuta_Aluguer.ID }, imovel_Venda_Permuta_Aluguer);
        }

        // DELETE: api/Imovel_Venda_Permuta_Aluguer/5
        [ResponseType(typeof(Imovel_Venda_Permuta_Aluguer))]
        public IHttpActionResult DeleteImovel_Venda_Permuta_Aluguer(int id)
        {
            Imovel_Venda_Permuta_AluguerDTO imovel_Venda_Permuta_Aluguer = repo.GetData(id);

            if (User.Identity.GetUserId() != imovel_Venda_Permuta_Aluguer.applicationUserID)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            repo.Delete(id);
            return Ok(imovel_Venda_Permuta_Aluguer);
        }
    }
}