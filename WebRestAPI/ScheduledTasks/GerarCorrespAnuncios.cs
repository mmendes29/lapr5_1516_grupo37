﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebRestAPI.ScheduledTasks
{
    public class GerarCorrespAnuncios : IJob
    {
        public void Execute(IJobExecutionContext cont)
        {
            var context = new ImoveisDbContext();
            List<Anuncio_Compra> anunciosCompra = context.Anuncio_Compra.ToList();
            List<Anuncio_Venda> anunciosVenda = context.Anuncio_Venda.ToList();

            foreach(Anuncio_Compra anuncioCompra in anunciosCompra)
                if(anuncioCompra.valido.Equals("true") && anuncioCompra.AnuncioVendaID != 0)
                    foreach(Anuncio_Venda anuncioVenda in anunciosVenda)
                        if(anuncioVenda.valido.Equals("true") && anuncioVenda.user != anuncioCompra.user &&
                            anuncioVenda.AnuncioCompraID != 0)

                            if(anuncioCompra.Imovel.tipo_de_imovel.nome == anuncioVenda.Imovel.tipo_de_imovel.nome &&
                            anuncioCompra.Imovel.localizacao.local == anuncioVenda.Imovel.localizacao.local &&
                            anuncioCompra.Imovel.areaMin <= anuncioVenda.Imovel.area &&
                            anuncioCompra.Imovel.areaMax >= anuncioVenda.Imovel.area &&
                            anuncioCompra.precoMin <= anuncioVenda.preco && anuncioCompra.precoMax >= anuncioVenda.preco)
                            {
                                anuncioCompra.AnuncioVendaID = anuncioVenda.ID;
                                anuncioCompra.correspValidada = "false";
                                anuncioVenda.AnuncioCompraID = anuncioCompra.ID;
                                anuncioVenda.correspValidada = "false";

                                context.SaveChanges();
                            }

            System.Diagnostics.Debug.WriteLine("[" + DateTime.Now.ToString("dd/MMMM/yy - H:mm:ss") + "]" + " -> Geração de correspondência entre anúncios iniciada");
        }
    }
}
