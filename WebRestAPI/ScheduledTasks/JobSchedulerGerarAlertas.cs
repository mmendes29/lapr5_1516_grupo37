﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Quartz;
using Quartz.Impl;
using System;

namespace WebRestAPI.ScheduledTasks
{
    public class JobSchedulerGerarAlertas
    {
        public static void Start()
        {
            try
            {
                var context = new ImoveisDbContext();
                AppConfig appConfig = context.AppConfigs.Find(1);

                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<GerarAlertaAnuncios>().Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger_GerarAlertaAnuncios")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(appConfig.IntervaloAlertas)
                        .RepeatForever())
                    .Build();

                scheduler.ScheduleJob(job, trigger);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("[" + DateTime.Now.ToString("dd/MMMM/yy - H:mm:ss") + "]" + " -> Erro ao gerar correspondência entre anúncios");
            }
        }
    }
}
