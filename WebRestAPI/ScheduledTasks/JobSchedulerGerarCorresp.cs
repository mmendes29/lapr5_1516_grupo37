﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Quartz;
using Quartz.Impl;
using System;

namespace WebRestAPI.ScheduledTasks
{
    public class JobSchedulerGerarCorresp
    {
        public static void Start()
        {
            try
            {
                var context = new ImoveisDbContext();
                AppConfig appConfig = context.AppConfigs.Find(1);

                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<GerarCorrespAnuncios>().Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger_GerarCorrespAnuncios")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(appConfig.IntervaloCorresp)
                        .RepeatForever())
                    .Build();

                scheduler.ScheduleJob(job, trigger);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("[" + DateTime.Now.ToString("dd/MMMM/yy - H:mm:ss") + "]" + " -> Erro ao gerar alertas de anúncios");
            }
        }
    }
}
