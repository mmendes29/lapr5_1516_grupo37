﻿using ClassLibrary.DAL;
using ClassLibrary.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebRestAPI.ScheduledTasks
{
    public class GerarAlertaAnuncios : IJob
    {
        public void Execute(IJobExecutionContext cont)
        {
            var context = new ImoveisDbContext();

            List<AnuncioAlerta> anunciosAlerta = context.AnunciosAlerta.ToList();
            if (anunciosAlerta.Count > 0)
            {
                foreach (var anuncioAlerta in anunciosAlerta)
                    context.AnunciosAlerta.Remove(anuncioAlerta);

                context.SaveChanges();
            }

            List<Alerta> alertas = context.Alertas.ToList();
            List<string> tipoAnuncio = new List<string>();
            List<string> tipoImovel = new List<string>();
            string local = null;
            float preçoMin = 0, preçoMax = 0, areaMin = 0, areaMax = 0;

            foreach (Alerta alerta in alertas)
            {
                foreach (var fixo in alerta.fixo)
                {
                    if (fixo.nome.Equals("TipoAnuncio"))
                        tipoAnuncio.Add(fixo.valor_string);
                    else
                        tipoImovel.Add(fixo.valor_string);
                }

                foreach (var paramAlfaNum in alerta.parametroAlfaNumerico)
                    if (paramAlfaNum.nome.Equals("local"))
                        local = paramAlfaNum.str;

                foreach (var paramNum in alerta.parametroIntervaloNumerico)
                {
                    if (paramNum.nome.Equals("area"))
                    {
                        areaMin = paramNum.min;
                        areaMax = paramNum.max;
                    }
                    else
                    {
                        preçoMin = paramNum.min;
                        preçoMax = paramNum.max;
                    }
                }

                if (tipoAnuncio.Contains("Compra"))
                {
                    List<Anuncio_Compra> anuncioCompra = context.Anuncio_Compra.ToList();
                    foreach (var anuncio in anuncioCompra)
                    {
                        if (tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.nome)
                            //|| tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.subTipo.nome))
                            && anuncio.Imovel.localizacao.local.Equals(local)
                            && anuncio.Imovel.areaMin >= areaMin && anuncio.Imovel.areaMax <= areaMax
                            && anuncio.precoMin >= preçoMin && anuncio.precoMax <= preçoMax)
                        {
                            AnuncioAlerta a = new AnuncioAlerta { AlertaId = alerta.ID, AnuncioId = anuncio.ID };
                            context.AnunciosAlerta.Add(a);
                            context.SaveChanges();
                        }
                    }
                }
                if (tipoAnuncio.Contains("Venda"))
                {
                    List<Anuncio_Venda> anuncioVenda = context.Anuncio_Venda.ToList();
                    foreach (var anuncio in anuncioVenda)
                    {
                        if (tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.nome)
                            //|| tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.subTipo.nome))
                            && anuncio.Imovel.localizacao.local.Equals(local)
                            && anuncio.Imovel.area >= areaMin && anuncio.Imovel.area <= areaMax
                            && anuncio.preco >= preçoMin && anuncio.preco <= preçoMax)
                        {
                            AnuncioAlerta a = new AnuncioAlerta { AlertaId = alerta.ID, AnuncioId = anuncio.ID };
                            context.AnunciosAlerta.Add(a);
                            context.SaveChanges();
                        }
                    }
                }
                if (tipoAnuncio.Contains("Permuta"))
                {
                    List<Anuncio_Permuta> anuncioPermuta = context.Anuncio_Permuta.ToList();
                    foreach (var anuncio in anuncioPermuta)
                    {
                        if (tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.nome)
                            && anuncio.Imovel.localizacao.local.Equals(local)
                            && anuncio.Imovel.area >= areaMin && anuncio.Imovel.area <= areaMax
                            && anuncio.preco >= preçoMin && anuncio.preco <= preçoMax)
                        {
                            AnuncioAlerta a = new AnuncioAlerta { AlertaId = alerta.ID, AnuncioId = anuncio.ID };
                            context.AnunciosAlerta.Add(a);
                            context.SaveChanges();
                        }
                    }
                }
                if (tipoAnuncio.Contains("Aluguer"))
                {
                    List<Anuncio_Aluguer> anuncioAluguer = context.Anuncio_Aluguer.ToList();
                    foreach (var anuncio in anuncioAluguer)
                    {
                        if (tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.nome)
                            // || tipoImovel.Contains(anuncio.Imovel.tipo_de_imovel.subTipo.nome))
                            && anuncio.Imovel.localizacao.local.Equals(local)
                            && anuncio.Imovel.area >= areaMin && anuncio.Imovel.area <= areaMax
                            && anuncio.renda >= preçoMin && anuncio.renda <= preçoMax)
                        {
                            AnuncioAlerta a = new AnuncioAlerta { AlertaId = alerta.ID, AnuncioId = anuncio.ID };
                            context.AnunciosAlerta.Add(a);
                            context.SaveChanges();
                        }
                    }
                }
            }
            System.Diagnostics.Debug.WriteLine("[" + DateTime.Now.ToString("dd/MMMM/yy - H:mm:ss") + "]" + " -> Geração de alerta de anúncios iniciada");
        }
    }
}