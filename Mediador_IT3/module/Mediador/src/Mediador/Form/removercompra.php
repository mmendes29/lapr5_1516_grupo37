<?php
namespace Mediador\Form;

use Zend\Form\Form;

class removercompra extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('compra');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'applicationUserID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'ImovelID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'tipoImovelID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'localizacaoID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'tipoImovel',
            'type' => 'Text',
            'options' => array(
                'label' => 'Tipo de Imovel: ',
            ),
        ));

        $this->add(array(
            'name' => 'local',
            'type' => 'Text',
            'options' => array(
                'label' => 'Local: '
            ),
        ));
        
        $this->add(array(
            'name' => 'latitude',
            'type' => 'Text',
            'options' => array(
                'label' => 'Latitude: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'longitude',
            'type' => 'Text',
            'options' => array(
                'label' => 'Longitude: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'areaMin',
            'type' => 'Text',
            'options' => array(
                'label' => 'Area Minima: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'areaMax',
            'type' => 'Text',
            'options' => array(
                'label' => 'Area Maxima: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'precoMin',
            'type' => 'Text',
            'options' => array(
                'label' => 'Preco Minimo: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'precoMax',
            'type' => 'Text',
            'options' => array(
                'label' => 'Preco Maximo: ',
            ),
        ));
        
        $this->add(array(
            'name' => 'valido',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'MediadorID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'AnuncioVendaID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
?>