<?php
namespace Mediador\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Debug\Debug;

use Mediador\Form\Credenciais;

set_time_limit(300);

class ImoServices
{
    const URL_Login = '/Token';
    const URL_Values = '/api/values';
    
    public static function Login(Credenciais $credenciais)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'] .self::URL_Login);

        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->username .'&password=' .$credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len 
        ));        
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        $response = $client->send();            
        
        $body=Json::decode($response->getBody());
        
        
        
        if(!empty($body->access_token))
        {
        if(!isset($_SESSION)){
                        session_start();
                    }

            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $credenciais->username;
            
            
            $roles = (array)ImoServices::getRole();
            
            if (in_array("[\"Mediador\"]", $roles) || in_array("[\"Admin\"]", $roles)) { 
                return true;
            } else {
                $_SESSION['access_token'] = null;
                $_SESSION['username'] = null;
                session_destroy();
                return false;
            }
        }
        else 
            return false;
    }

    public static function getRole()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        
        $client = new Client("https://10.8.11.116:44301/api/Role");
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
    
    public static function Register($username,$password)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
    
        $client = new Client("https://10.8.11.116:44301/api/Account/Register");
    
        $client->setMethod(Request::METHOD_POST);
        
        
        $params = 'app=mediador&Email=' . $username .'&Password=' . $password .'&ConfirmPassword=' .$password;
        
        
        $len = strlen($params);
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len 
        ));
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        
        $response = $client->send();
    
        $body=Json::decode($response->getBody());
        
    
        return $body;
    }
    
    public static function Logout()
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }
    
    public static function getValues()
    {
    if(!isset($_SESSION)){
                        session_start();
                    }

        $client = new Client('https://' .$_SESSION['server'] .self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);

        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
    
    public static function getAllAnuncio($anuncio)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client("https://10.8.11.116:44301/api/".$anuncio);
        $client->setMethod(Request::METHOD_GET);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
    
        $client->setOptions(['sslverifypeer' => false]);
        $response = $client->send();
        return Json::decode($response->getBody(), true);
    
    }
    
    
    public static function getAnuncio($anuncio,$id)
    {
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client("https://" .$_SESSION['server'] ."/api/" . $anuncio . "/" . $id);
        $client->setMethod(Request::METHOD_GET);
    
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
    
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        return Json::decode($response->getBody(), true);
    
    
    }
    
    public static function editAnuncio($id, $tipo, $data){
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_PUT);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
             'Authorization'   => $bearer_token,
        ));
    
        $encoded = Json::encode($data);
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
    
        $response = $client->send();
        
        return true;
    }
    
    public static function validaAnuncio($id, $tipo, $data){
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_PUT);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
    
        $data['valido'] = 'true';
        $encoded = Json::encode($data);
        
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($encoded);
    
        $response = $client->send();
    
        return true;
    }
    
    public static function removeAnuncio($id, $tipo){
    if(!isset($_SESSION)){
                        session_start();
                    }
        $client = new Client('https://' .$_SESSION['server'].'/api/'.$tipo.'/'.$id);
    
        $client->setMethod(Request::METHOD_DELETE);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
    
    
        $client->setOptions(['sslverifypeer' => false]);
    
        $response = $client->send();
    
        return true;
    }
    
    public static function associarVendaCompra($id,$idCompra)
    {
        
        $venda = ImoServices::getAnuncio("Anuncio_Venda", $id);
        $compra = ImoServices::getAnuncio("Anuncio_Compra", $idCompra);
        
        $venda['Anuncio_Compra'] = $compra;
        $compra['Anuncio_Venda'] = $venda;
        
        ImoServices::editAnuncio($id, "Anuncio_Venda", $venda);
        ImoServices::editAnuncio($idCompra, "Anuncio_Compra", $compra);
        
        return true;
    }
    
    public static function corresponderValidade($id,$idCompra)
    {
    
       $venda = ImoServices::getAnuncio("Anuncio_Venda", $id);
       $compra = ImoServices::getAnuncio("Anuncio_Compra", $idCompra);
    
      //  $venda['correspValidada'] = 'true';
      //  $compra['correspValidada'] = 'true';
    
     //   ImoServices::editAnuncio($id, "Anuncio_Venda", $venda);
     //   ImoServices::editAnuncio($idCompra, "Anuncio_Compra", $compra);
        
        if(!isset($_SESSION)){
            session_start();
        }
        $clientVenda = new Client('https://' .$_SESSION['server'].'/api/Anuncio_Venda/'.$id);
        $clientCompra = new Client('https://' .$_SESSION['server'].'/api/Anuncio_Compra/'.$idCompra);
        
        $clientVenda->setMethod(Request::METHOD_PUT);
        $clientCompra->setMethod(Request::METHOD_PUT);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $clientVenda->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
        $clientCompra->setHeaders(array(
            'Content-Type'   => 'application/json',
            'Authorization'   => $bearer_token,
        ));
        
        $venda['correspValidada'] = 'true';
        $encoded = Json::encode($venda);
        
        
        $clientVenda->setOptions(['sslverifypeer' => false]);
        $clientVenda->setRawBody($encoded);
        
        $response = $clientVenda->send();
        
        $compra['correspValidada'] = 'true';
        $encoded = Json::encode($compra);
        
        
        $clientCompra->setOptions(['sslverifypeer' => false]);
        $clientCompra->setRawBody($encoded);
        
        $response = $clientCompra->send();
        
        return true;
        }
        
        public static function getAllPedidos()
        {
            if(!isset($_SESSION)){
                session_start();
            }
            $client = new Client("https://10.8.11.116:44301/api/PedidoVisita");
            $client->setMethod(Request::METHOD_GET);
        
            $bearer_token = 'Bearer ' . $_SESSION['access_token'];
            $client->setHeaders(array(
                'Authorization'   => $bearer_token,
            ));
        
            $client->setOptions(['sslverifypeer' => false]);
            $response = $client->send();
            return Json::decode($response->getBody(), true);
        
        
        }
        

}

?>