 <?php

 $title = 'MEDIADOR Validar os Anuncios';
 $this->headTitle($title);
 ?>
<br/>
<h2>Corresponder Venda - Compra </h2>

<br/>
  
  <?php
$compraf=false;
$vendaf=false;
foreach ($compra as $anuncioCompra) {

    if($anuncioCompra['valido']=="true" && $anuncioCompra['AnuncioVendaID']==0)
    {
        $compraf=true;
    }
}

foreach ($venda as $anuncioVenda) {
    if($anuncioVenda['valido']=="true" && $anuncioVenda['AnuncioCompraID']==0)
        {
               $vendaf=true;
    }
}

?>

<?php 
    if ($vendaf == true && $compraf == false)
    {
        echo "<h4>Nao existem Anuncios Compra</h4>";
    } else if ($vendaf == false && $compraf == true)
    {
        echo "<h4>Nao existem Anuncios Venda</h4>";
    } else if ($vendaf == false && $compraf == false)
    {
        echo "<h4>Nao existem Anuncios Compra nem Anuncios Venda</h4>";
    }
?>

 
 <br/>
 
 <?php 
 if ($vendaf == true && $compraf == true)
 {
     echo "<h4>Anuncios de Compra</h4>";
 }
 ?>
<table class="table" >
  <?php 
  if ($vendaf == true && $compraf == true)
  {
  echo "<th>";
  echo "ID";
  echo "</th>";
  echo "<th>";
  echo "Tipo de Imovel";
  echo "</th>";
  echo "<th>";
  echo "Local";
  echo "</th>";
  echo "<th>";
  echo "Latitude";
  echo "</th>";
  echo "<th>";
  echo "Longitude";
  echo "</th>";
  echo "<th>";
  echo "Area Minima";
  echo "</th>";
  echo "<th>";
  echo "Area Maxima";
  echo "</th>";
  echo "<th>";
  echo "Preco Minima";
  echo "</th>";
  echo "<th>";
  echo "Preco Maxima";
  echo "</th>";
    foreach ($compra as $anuncioCompra) {
        if($anuncioCompra['valido']=="true" && $anuncioCompra['AnuncioVendaID']==0)
        {
    echo "<tr>";
    
        echo "<td>";
        echo $anuncioCompra['ID'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['tipoImovel'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['local'];
        echo "</td>";

        echo "<td>";
        echo $anuncioCompra['latitude'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['longitude'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['areaMin'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['areaMax'];
        echo "</td>";        
        
        echo "<td>";
        echo $anuncioCompra['precoMin'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioCompra['precoMax'];
        echo "</td>";
       
    echo "</tr>";
        }
    }
  }
?>
</table>

<?php 
if ($vendaf == true && $compraf == true)
 {
 echo "<h4>Anuncios de Venda</h4>";
}?>
  
  <table class="table">
  <?php 
  if ($vendaf == true && $compraf == true)
  {
  echo "<th>";
  echo "ID";
  echo "</th>";
  echo "<th>";
  echo "Tipo de Imovel";
  echo "</th>";
  echo "<th>";
  echo "Local";
  echo "</th>";
  echo "<th>";
  echo "Latitude";
  echo "</th>";
  echo "<th>";
  echo "Longitude";
  echo "</th>";
  echo "<th>";
  echo "Area";
  echo "</th>";
  echo "<th>";
  echo "Preco";
  echo "</th>";
    foreach ($venda as $anuncioVenda) {
        if($anuncioVenda['valido']=="true" && $anuncioVenda['AnuncioCompraID']==0)
        {
    echo "<tr>";
        
        echo "<td>";
        echo $anuncioVenda['ID'];
        echo "</td>";
    
        echo "<td>";
        echo $anuncioVenda['tipoImovel'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioVenda['local'];
        echo "</td>";

        echo "<td>";
        echo $anuncioVenda['latitude'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioVenda['longitude'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioVenda['area'];
        echo "</td>";
        
        echo "<td>";
        echo $anuncioVenda['preco'];
        echo "</td>";
   

    echo "</tr>"; 
        }
    }
  }

    ?>

  </table>

 <?php  
 if ($vendaf == true && $compraf == true)
 {
 $form->setAttribute('action', $this->url('mediador', array('action' => 'correspondenciavalidada','anuncioC' => 'compraBtn')));
 $form->prepare();
 echo $this->form()->openTag($form);
 echo $this->formRow($form->get('AnuncioCompraID'));
 echo "<br/>";
 echo $this->formRow($form->get('AnuncioVendaID'));
 echo "<br/>"; echo "<br/>";
 echo $this->formSubmit($form->get('submit'));
 echo $this->form()->closeTag();
 }
 ?>
