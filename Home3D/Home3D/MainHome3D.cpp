#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <Al/alut.h>
#endif

#ifdef _WIN32
#include <GL/glaux.h>
#include <GL/GL.h>
#endif

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include "iostream"
#include "fstream"
#include "glm.h"

using namespace std;

#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib usada para ler bitmaps */

// fun�cao para ler jpegs do ficheiro readjpeg.c
extern "C" int read_JPEG_file(const char *, char **, int *, int *, int *);

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)					(M_PI*(x)/180)
#define GRAUS(x)				(180*(x)/M_PI)

#define	GAP						25

#define BLUEPRINT_HEIGHT		16
#define BLUEPRINT_WIDTH			17

#define	OBJECTO_ALTURA			0.4
#define OBJECTO_VELOCIDADE		0.5
#define OBJECTO_ROTACAO			5
#define OBJECTO_RAIO			0.12
#define SCALE_VISITOR			0.010
#define EYE_ROTACAO				1

#ifdef _WIN32

#else
#define NOME_TEXTURA_CUBOS        "branco.jpg"
#endif

#define NUM_TEXTURAS			5
#define ID_TEXTURA_PAREDES		0
#define ID_TEXTURA_CHAO			1
#define ID_TEXTURA_TETO 		2
#define ID_TEXTURA_PORTAS_V		3
#define ID_TEXTURA_PORTAS_H		4

#define	CHAO_DIMENSAO			10
#define NUM_JANELAS				2
#define JANELA_TOP				0
#define JANELA_TOP2				5
#define JANELA_NAVIGATE			1
#define JANELA_NAVIGATE2		1

GLuint objectList;

typedef struct teclas_t {
	GLboolean   up, down, left, right;
}teclas_t;

typedef struct pos_t {
	GLfloat    x, y, z;
}pos_t;

typedef struct objecto_t {
	pos_t    pos;
	GLfloat  dir;
	GLfloat  vel;
}objecto_t;

typedef struct camera_t {
	pos_t    eye;
	GLfloat  dir_long;  // longitude olhar (esq-dir)
	GLfloat  dir_lat;   // latitude olhar	(cima-baixo)
	GLfloat  fov;
}camera_t;

typedef struct ESTADO {
	camera_t      camera;
	GLint         timer;
	GLint         mainWindow, topSubwindow, navigateSubwindow;
	teclas_t      teclas;
	GLboolean     localViewer;
	GLuint        vista[NUM_JANELAS];
	ALuint		  buffer, source;
	ALboolean	  tecla_m;
	ALboolean	  tecla_o;
}ESTADO;

typedef struct MODELO {
	GLuint        texID[NUM_JANELAS][NUM_TEXTURAS];
	GLuint        house[NUM_JANELAS];
	GLuint        chao[NUM_JANELAS];
	objecto_t	  objecto;
	GLuint        xMouse;
	GLuint        yMouse;
	StudioModel   visitor[NUM_JANELAS];
	GLboolean     andar;
	GLuint        prev;
	int			  duracao;
}MODELO;


//variaveis globais
ESTADO estado;
MODELO modelo;
vector<string> texturas;
vector<string> mobiliario;
char blueprintData1[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1];
char blueprintData2[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1];

const char *objecto;
char *objetosimples;
GLuint VerticalWindowList,
HorizontalWindowList, BedroomList, BathRoomList, OfficeRoomList;

GLMmodel* obj;
GLMmodel* temp = NULL;

void LoadMatrix(const char * f, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1])
{
	int c;
	FILE *file;
	char n;
	file = fopen(f, "r");
	if (!file)
	{
		cerr << "ERRO: Ficheiro " << f << " nao foi encontrado.\n" << endl;
		system("pause");
		exit(0);
	}
	else
	{
		while ((c = getc(file)) != EOF)
			for (int i = 0; i < BLUEPRINT_HEIGHT; i++)
				for (int j = 0; j < BLUEPRINT_WIDTH + 1; j++)
					if (!fscanf(file, "%c", &data[i][j]))
						printf("%c\n", data[i][j]);
	}
	fclose(file);
}

void LoadTexturas()
{
	ifstream fx;
	fx.open("Data/Texturas.txt");

	if (!fx)
	{
		cerr << "Erro: Ficheiro Texturas.txt nao foi encontrado.\n" << endl;
		system("pause");
		exit(0);
	}
	while (!fx.eof())
	{
		string linha;
		int ini = 0, pos = 0, count = 0;
		getline(fx, linha, '\n');
		while (pos < linha.size())
		{
			pos = linha.find(';', ini);
			string textura = linha.substr(ini, pos - ini);
			if (texturas.size() == count + 1)
				texturas.resize(count + 1);
			texturas.push_back(textura);
			pos++;
			count++;
			ini = pos;
		}
	}
	fx.close();
}

// Ilumina��o e materiais
void setLight()
{
	GLfloat light_pos[4] = { -5.0, 20.0, -8.0, 0.0 };
	GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };

	// ligar ilumina��o
	glEnable(GL_LIGHTING);

	// ligar e definir fonte de luz 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.localViewer);
}

void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;

	// cria��o autom�tica das componentes Ambiente e Difusa do material a partir das cores
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros par�metros dos materiais est�ticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

//// Redisplays
void redisplayTopSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint)width, (GLint)height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLfloat)width / height, .5, 100);
	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);

}

void reshapeNavigateSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint)width, (GLint)height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera.fov, (GLfloat)width / height, 0.1, 50);
	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}

void reshapeMainWindow(int width, int height)
{
	GLint w, h;
	w = (width - GAP * 3)*.5;
	h = (height - GAP * 2);
	glutSetWindow(estado.topSubwindow);
	glutPositionWindow(GAP, GAP);
	glutReshapeWindow(w, h);
	glutSetWindow(estado.navigateSubwindow);
	glutPositionWindow(GAP + w + GAP, GAP);
	glutReshapeWindow(w, h);
}

void strokeCenterString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glTranslated(x - glutStrokeLength(GLUT_STROKE_ROMAN, (const unsigned char*)str)*0.5*s, y - 119.05*0.5*s, z);
	glScaled(s, s, s);
	for (i = 0; i<n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);
	glPopMatrix();
}

//Modelo
GLboolean detectaColisao(GLfloat nx, GLfloat nz)
{
	int i = (int)(nx + BLUEPRINT_HEIGHT * 0.5 + 0.5);
	int j = (int)(nz + BLUEPRINT_WIDTH * 0.5 + 0.5);
	int k = (int)(nx + BLUEPRINT_HEIGHT * 0.3 + 0.3);
	int l = (int)(nz + BLUEPRINT_WIDTH * 0.3 + 0.3);
	if (blueprintData1[i][j] == '|' || blueprintData1[i][j] == '-' || blueprintData1[i][j] == 'J' ||
		blueprintData1[i][j] == 'j' || blueprintData1[i][j] == 'W' || blueprintData1[i][j] == 'w')
	{
		if (modelo.visitor[JANELA_NAVIGATE].GetSequence() != 20)
		{
			modelo.visitor[JANELA_NAVIGATE].SetSequence(0);
			modelo.visitor[JANELA_TOP].SetSequence(0);
			//modelo.duracao = 1166;
		}
		return GL_TRUE;
		if (blueprintData1[5][15]) {
			
		}
	
	}
	return GL_FALSE;
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat s, GLfloat t)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(s, t);
	glVertex3fv(a);
	glTexCoord2f(s + 1, t);
	glVertex3fv(b);
	glTexCoord2f(s + 1, t + 1);
	glVertex3fv(c);
	glTexCoord2f(s, t + 1);
	glVertex3fv(d);
	glEnd();
}

void desenhaParedeVertical(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -1, -0.5, -0.05 },
		{ 1, -0.5, -0.05 },
		{ 1, 0.5, -0.05 },
		{ -1, 0.5, -0.05 },
		{ -1, -0.5, 0.05 },
		{ 1, -0.5, 0.05 },
		{ 1, 0.5, 0.05 },
		{ -1, 0.5, 0.05 }
	};
	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaParedeHorizontal(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -0.05, -0.5, -1 },
		{ 0.05, -0.5, -1 },
		{ 0.05, 0.5, -1 },
		{ -0.05, 0.5, -1 },
		{ -0.05, -0.5, 1 },
		{ 0.05, -0.5, 1 },
		{ 0.05, 0.5, 1 },
		{ -0.05, 0.5, 1 }
	};
	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaPlanoHorizontal(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -0.5, -0.001, -0.5 },
		{ 0.5, -0.001, -0.5 },
		{ 0.5, 0.001, -0.5 },
		{ -0.5, 0.001, -0.5 },
		{ -0.5, -0.001, 0.5 },
		{ 0.5, -0.001, 0.5 },
		{ 0.5, 0.001, 0.5 },
		{ -0.5, 0.001, 0.5 }
	};
	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaPortaVertical(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -1, -0.5, -0.02 },
		{ 1, -0.5, -0.02 },
		{ 1, 0.5, -0.02 },
		{ -1, 0.5, -0.02 },
		{ -1, -0.5, 0.02 },
		{ 1, -0.5, 0.02 },
		{ 1, 0.5, 0.02 },
		{ -1, 0.5, 0.02 }
	};

	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaPortaHorizontal(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -0.02, -0.5, -1 },
		{ 0.02, -0.5, -1 },
		{ 0.02, 0.5, -1 },
		{ -0.02, 0.5, -1 },
		{ -0.02, -0.5, 1 },
		{ 0.02, -0.5, 1 },
		{ 0.02, 0.5, 1 },
		{ -0.02, 0.5, 1 }
	};

	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaJanelaVertical(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -0.5, -0.15, -0.05 },
		{ 0.5, -0.15, -0.05 },
		{ 0.5, 0.15, -0.05 },
		{ -0.5, 0.15, -0.05 },
		{ -0.5, -0.15, 0.05 },
		{ 0.5, -0.15, 0.05 },
		{ 0.5, 0.15, 0.05 },
		{ -0.5, 0.15, 0.05 }
	};
	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaJanelaHorizontal(GLuint texID, GLfloat s, GLfloat t)
{
	GLfloat vertices[][3] =
	{
		{ -0.05, -0.15, -0.5 },
		{ 0.05, -0.15, -0.5 },
		{ 0.05, 0.15, -0.5 },
		{ -0.05, 0.15, -0.5 },
		{ -0.05, -0.15, 0.5 },
		{ 0.05, -0.15, 0.5 },
		{ 0.05, 0.15, 0.5 },
		{ -0.05, 0.15, 0.5 }
	};
	GLfloat normais[][3] =
	{
		{ 0, 0, -1 },
		{ 0, 1, 0 },
		{ -1, 0, 0 },
		{ 1, 0, 0 },
		{ 0, 0, 1 },
		{ 0, -1, 0 }
		// acrescentar as outras normais...
	};

	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[0], s, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[0], s, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[0], s, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[0], s, t);

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaBussola(int width, int height)  // largura e altura da janela
{

	// Altera viewport e projec��o para 2D (copia de um reshape de um projecto 2D)

	//....

	// Blending (transparencias)
	/*  glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	*/

	//desenha bussola 2D

	//glColor3f(1,0.4,0.4);
	//strokeCenterString("N", 0, 20, 0 , 0.1); // string, x ,y ,z ,scale


	// rop�e estado
	/* glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
	*/

	//rep�e projec��o chamando redisplay
	reshapeNavigateSubwindow(width, height);

}

void desenhaModeloDir(objecto_t obj, int width, int height)
{
	// Altera viewport e projec��o
	//....

	// Blending (transparencias)
	/*
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	*/

	//desenha Seta

	// rop�e estado
	/*  glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	*/
	//rep�e projec��o chamando redisplay
	redisplayTopSubwindow(width, height);
}

void desenhaAngVisao(camera_t *cam)
{
	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT); // propor��o 
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glPushMatrix();
	glTranslatef(cam->eye.x, OBJECTO_ALTURA, cam->eye.z);
	glColor4f(0, 0, 1, 0.2);
	glRotatef(GRAUS(cam->dir_long), 0, 1, 0);

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, -5 * sin(RAD(cam->fov*ratio*0.5)));
	glVertex3f(5 * cos(RAD(cam->fov*ratio*0.5)), 0, 5 * sin(RAD(cam->fov*ratio*0.5)));
	glEnd();
	glPopMatrix();

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void desenhaModelo()
{
	glColor3f(0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(0, OBJECTO_ALTURA*0.75, 0);
	glRotatef(GRAUS(estado.camera.dir_long - modelo.objecto.dir), 0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA*0.5);
	glPopMatrix();
}

void desenhaPlanta(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.8, 0.8, 0.8);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == '|')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				desenhaParedeVertical(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}

			if (data[i][j] == '-')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				desenhaParedeHorizontal(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
		}
	}
}
void desenhaS(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura) {
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.5f, 0.5f, 0.5f);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (i == 2 & j == 4) {
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				obj = glmReadOBJ("obj/Quarto1/BED.obj");
				//glmLinearTexture(obj);
				glmUnitize(obj);
				glmFacetNormals(obj);
				glmVertexNormals(obj, 45.0);
				glmScale(obj, 0.75);
				glTranslatef(-1, -0.25, -2);
				//glRotatef(90, 0, 1, 0);
				glmDraw(obj, GLM_SMOOTH);
				glmDelete(obj);
				glPopMatrix();
			}
			if (i == 5 & j == 10) {
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glCallList(OfficeRoomList);
				glPopMatrix();
			}

			/*if (data[i][j] == 'R')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glCallList(OfficeRoomList);
				glPopMatrix();
			}*/
		}
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}
void desenhaObj(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura) {
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.5f, 0.5f, 0.5f);

	ifstream fx;
	fx.open("Data/mobiliario.txt");

	if (!fx)
	{
		cerr << "Erro: Ficheiro Mobiliario.txt nao foi encontrado.\n" << endl;
		system("pause");
		exit(0);
	}
	while (!fx.eof())
	{
		string linha;
		int ini = 0, pos = 0;
		getline(fx, linha, '\n');
		while (pos < linha.size())
		{
			pos = linha.find(':', ini);
			string nome = linha.substr(ini, pos - ini);
			char *testeObj = new char[nome.length() + 1];
			//const char *testeObj = nome.c_str();
			strcpy(testeObj, nome.c_str());
			testeObj[nome.size()] = '\0';
			
			pos++;
			ini = pos;

			pos = linha.find(':', ini);
			int piso = atoi(linha.substr(ini, pos - ini).c_str());
			pos++;
			ini = pos;

			pos = linha.find(':', ini);
			int orientacao = atoi(linha.substr(ini, pos - ini).c_str());
			pos++;
			ini = pos;

			pos = linha.find(':', ini);
			int posx = atoi(linha.substr(ini, pos - ini).c_str());
			pos++;
			ini = pos;

			pos = linha.find(':', ini);
			int posy = atoi(linha.substr(ini, pos - ini).c_str());

			// CODIGO PARA DESENHAR OBJETO //
			for (i = 0; i < BLUEPRINT_HEIGHT; i++)
			{
				for (j = 0; j < BLUEPRINT_WIDTH; j++)
				{
					if (posx == i & posy == j) {
						
						glPushMatrix();
						glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
						//obj = glmReadOBJ((char*)(nomeFicheiroObj));
						//obj = glmReadOBJ(((char*)nome.c_str()));
						obj = glmReadOBJ("obj/Desk.obj");
						//glmLinearTexture(obj);
						glmUnitize(obj);
						glmFacetNormals(obj);
						glmVertexNormals(obj, 45.0);
						glmScale(obj, 0.75);
						glTranslatef(-1, -0.25, -2);
						//glRotatef(90, 0, 1, 0);
						glmDraw(obj, GLM_SMOOTH);
						glmDelete(obj);
						glPopMatrix();
					}
				}
			}
			glBindTexture(GL_TEXTURE_2D, NULL);
			delete[] testeObj;
		}
	}
	fx.close();
}

void desenhaOffice(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura) {
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.5f, 0.5f, 0.5f);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'E')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glCallList(OfficeRoomList);
				glPopMatrix();
			}
		}
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaChao(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.5f, 0.5f, 0.5f);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'x')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				desenhaPlanoHorizontal(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
		}
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaTeto(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.5f, 0.5f, 0.5f);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'x')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				desenhaPlanoHorizontal(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
		}
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaPortasVerticais(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.8, 0.8, 0.8);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'P')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(-0.75, 0, 0);
				glRotatef(180, 0, 0, 1);
				glScalef(0.25, 1, 1);
				desenhaPortaVertical(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
			if (data[i][j] == 'p')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0.75, 0, 0);
				glRotatef(180, 0, 1, 0);
				glRotatef(180, 0, 0, 0);
				glScalef(0.25, 1, 1);
				desenhaPortaVertical(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
		}
	}
}

void desenhaPortasHorizontais(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.8, 0.8, 0.8);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'O')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0, 0, -0.75);
				glScalef(1, 1, 0.25);
				desenhaPortaHorizontal(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
			if (data[i][j] == 'o')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0, 0, 0.75);
				glRotatef(180, 1, 0, 0);
				glRotatef(180, 0, 0, 0);
				glScalef(1, 1, 0.25);
				desenhaPortaHorizontal(texID, (i % 4) * 1, (j % 4) * 1);
				glPopMatrix();
			}
		}
	}
}

void desenhaJanelas(GLuint texID, const char * file, char data[BLUEPRINT_HEIGHT][BLUEPRINT_WIDTH + 1], double altura)
{
	

	LoadMatrix(file, data);
	int i, j;
	glColor3f(0.8, 0.8, 0.8);

	for (i = 0; i < BLUEPRINT_HEIGHT; i++)
	{
		for (j = 0; j < BLUEPRINT_WIDTH; j++)
		{
			if (data[i][j] == 'J')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(-0.5, -0.35, 0);
				//desenhaJanelaVertical(texID, (i % 4) * 0.5, (j % 4) * 0.5);
				glCallList(VerticalWindowList);
				glPopMatrix();
			}
			if (data[i][j] == 'j')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0.5, 0.35, 0);
				//desenhaJanelaVertical(texID, (i % 4) * 0.5, (j % 4) * 0.5);
				glCallList(VerticalWindowList);
				glPopMatrix();
			}
			if (data[i][j] == 'W')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0, -0.35, -0.5);
				//desenhaJanelaHorizontal(texID, (i % 4) * 0.5, (j % 4) * 0.5);
				glCallList(HorizontalWindowList);
				glPopMatrix();
			}
			if (data[i][j] == 'w')
			{
				glPushMatrix();
				glTranslatef(i - 0.5 * BLUEPRINT_HEIGHT, altura, j - 0.5 * BLUEPRINT_WIDTH);
				glTranslatef(0, 0.35, 0.5);
				//desenhaJanelaHorizontal(texID, (i % 4) * 0.5, (j % 4) * 0.5);
				glCallList(HorizontalWindowList);
				glPopMatrix();
			}
		}
	}
}

//navigateSubwindow
void motionNavigateSubwindow(int x, int y)
{
	int dif;
	dif = x - (int)modelo.xMouse;
	estado.camera.dir_long -= dif * RAD(EYE_ROTACAO);
	dif = y - (int)modelo.yMouse;
	estado.camera.dir_lat -= dif * RAD(EYE_ROTACAO);

	// LIMITAR ROTACAO +/- 45 GRAUS
	modelo.xMouse = x;
	modelo.yMouse = y;
}

void mouseNavigateSubwindow(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON)
		if (state == GLUT_DOWN)
		{
			modelo.xMouse = x;
			modelo.yMouse = y;
			glutMotionFunc(motionNavigateSubwindow);
		}
		else
			glutMotionFunc(NULL);
}

void setNavigateSubwindowCamera(camera_t *cam, objecto_t obj)
{

	pos_t center;
	// 1� PESSOA
	if (estado.vista[JANELA_NAVIGATE])
	{

		cam->eye.x = obj.pos.x;
		cam->eye.y = obj.pos.y + 0.2;
		cam->eye.z = obj.pos.z;
		center.x = cam->eye.x + 1 * cos(-cam->dir_long) * cos(cam->dir_lat);
		center.y = cam->eye.y + 1 * sin(cam->dir_lat);
		center.z = cam->eye.z + 1 * sin(-cam->dir_long) * cos(cam->dir_lat);

	}
	// 3� PESSOA
	else
	{
		center.x = obj.pos.x;
		center.y = obj.pos.y + 0.1;
		center.z = obj.pos.z;
		cam->eye.x = center.x - 1 * cos(-cam->dir_long);
		cam->eye.y = center.y + 0.5;
		cam->eye.z = center.z - 1 * sin(-cam->dir_long);
	}

	gluLookAt(cam->eye.x, cam->eye.y, cam->eye.z, center.x, center.y, center.z, 0, 1, 0);
}

void displayNavigateSubwindow()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	setNavigateSubwindowCamera(&estado.camera, modelo.objecto);
	setLight();

	glCallList(modelo.house[JANELA_NAVIGATE]);
	glCallList(modelo.chao[JANELA_NAVIGATE]);

	if (!estado.vista[JANELA_NAVIGATE])
	{
		glPushMatrix();
		glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
		glRotatef(-90, 1, 0, 0);
		glScalef(SCALE_VISITOR, SCALE_VISITOR, SCALE_VISITOR);
		mdlviewer_display(modelo.visitor[JANELA_NAVIGATE]);
		glPopMatrix();
	}

	desenhaBussola(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glutSwapBuffers();
}

//topSubwindow
void setTopSubwindowCamera(camera_t *cam, objecto_t obj)
{
	cam->eye.x = obj.pos.x;
	cam->eye.z = obj.pos.z;
	if (estado.vista[JANELA_TOP])
		gluLookAt(obj.pos.x, CHAO_DIMENSAO*.2, obj.pos.z, obj.pos.x, obj.pos.y, obj.pos.z, 0, 0, -1);
	else
		gluLookAt(obj.pos.x, CHAO_DIMENSAO * 2, obj.pos.z, obj.pos.x, obj.pos.y, obj.pos.z, 0, 0, -1);
}

void displayTopSubwindow()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	setTopSubwindowCamera(&estado.camera, modelo.objecto);
	setLight();

	glCallList(modelo.house[JANELA_TOP]);
	glCallList(modelo.chao[JANELA_TOP]);

	glPushMatrix();
	glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
	glRotatef(GRAUS(modelo.objecto.dir), 0, 1, 0);
	glRotatef(-90, 1, 0, 0);
	glScalef(SCALE_VISITOR, SCALE_VISITOR, SCALE_VISITOR);
	mdlviewer_display(modelo.visitor[JANELA_TOP]);
	glPopMatrix();

	desenhaAngVisao(&estado.camera);
	desenhaModeloDir(modelo.objecto, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glutSwapBuffers();
}

//mainWindow
void redisplayAll(void)
{
	glutSetWindow(estado.mainWindow);
	glutPostRedisplay();
	glutSetWindow(estado.topSubwindow);
	glutPostRedisplay();
	glutSetWindow(estado.navigateSubwindow);
	glutPostRedisplay();
}

void displayMainWindow()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glutSwapBuffers();
}

void Timer(int value)
{
	GLfloat nx = 0, nz = 0;
	GLboolean andar = GL_FALSE;

	GLuint curr = glutGet(GLUT_ELAPSED_TIME);
	// calcula velocidade baseado no tempo passado
	float velocidade = modelo.objecto.vel*(curr - modelo.prev)*0.004;

	glutTimerFunc(estado.timer, Timer, 0);
	if (modelo.duracao > 0)
	{
		modelo.duracao -= (int)curr - (int)modelo.prev;
		modelo.prev = curr;
		redisplayAll();
		return;
	}

	modelo.prev = curr;

	if (estado.teclas.up) {
		// calcula nova posi��o nx,nz
		nx = modelo.objecto.pos.x + velocidade * cos(-modelo.objecto.dir);
		nz = modelo.objecto.pos.z + velocidade * sin(-modelo.objecto.dir);
		if (!detectaColisao(nx + OBJECTO_RAIO * cos(-modelo.objecto.dir), nz + OBJECTO_RAIO * sin(-modelo.objecto.dir))
			&& !detectaColisao(nx + OBJECTO_RAIO * cos(-modelo.objecto.dir - RAD(45)), nz + OBJECTO_RAIO * sin(-modelo.objecto.dir - RAD(45)))
			&& !detectaColisao(nx + OBJECTO_RAIO * cos(-modelo.objecto.dir + RAD(45)), nz + OBJECTO_RAIO * sin(-modelo.objecto.dir + RAD(45))))
		{
			modelo.objecto.pos.x = nx;
			modelo.objecto.pos.z = nz;
		}
		andar = GL_TRUE;
	}

	if (estado.teclas.down) {
		// calcula nova posi��o nx,nz
		nx = modelo.objecto.pos.x - velocidade * cos(-modelo.objecto.dir);
		nz = modelo.objecto.pos.z - velocidade * sin(-modelo.objecto.dir);
		if (!detectaColisao(nx - OBJECTO_RAIO * cos(-modelo.objecto.dir), nz - OBJECTO_RAIO * sin(-modelo.objecto.dir))
			&& !detectaColisao(nx - OBJECTO_RAIO * cos(-modelo.objecto.dir - RAD(45)), nz - OBJECTO_RAIO * sin(-modelo.objecto.dir - RAD(45)))
			&& !detectaColisao(nx - OBJECTO_RAIO * cos(-modelo.objecto.dir + RAD(45)), nz - OBJECTO_RAIO * sin(-modelo.objecto.dir + RAD(45))))
		{
			modelo.objecto.pos.x = nx;
			modelo.objecto.pos.z = nz;
		}
		andar = GL_TRUE;
	}
	if (estado.teclas.left) {
		// rodar camara e objecto
		modelo.objecto.dir += RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long += RAD(OBJECTO_ROTACAO);
	}
	if (estado.teclas.right) {
		// rodar camara e objecto
		modelo.objecto.dir -= RAD(OBJECTO_ROTACAO);
		estado.camera.dir_long -= RAD(OBJECTO_ROTACAO);
	}

	// Sequencias - 0(parado) 3(andar) 20(choque)
	//  modelo.homer[JANELA_NAVIGATE].GetSequence()  le Sequencia usada pelo homer
	//  modelo.homer[JANELA_NAVIGATE].SetSequence()  muda Sequencia usada pelo homer

	if (andar)
	{
		if (modelo.visitor[JANELA_NAVIGATE].GetSequence() != 3 && modelo.visitor[JANELA_NAVIGATE].GetSequence() != 20)
		{
			modelo.visitor[JANELA_NAVIGATE].SetSequence(3);
			modelo.visitor[JANELA_TOP].SetSequence(3);
		}
	}
	else
	{
		if (modelo.visitor[JANELA_NAVIGATE].GetSequence() != 0)
		{
			modelo.visitor[JANELA_NAVIGATE].SetSequence(0);
			modelo.visitor[JANELA_TOP].SetSequence(0);
		}
	}
	//Som
	ALint state;

	alGetSourcei(estado.source, AL_SOURCE_STATE, &state);
	if (estado.tecla_m)
	{
		if (state != AL_PLAYING)
			alSourcePlay(estado.source);
	}
	else
		if (state == AL_PLAYING)
			alSourceStop(estado.source);
	
	/*if (estado.tecla_o) {
		if (state != AL_STOPPED)
			alSourceStop(estado.source);
	}
	else if (state == AL_STOPPED)
		alSourcePlay(estado.source);*/

	redisplayAll();
}

void imprime_ajuda(void)
{
	printf("\n\nDesenho de um quadrado\n");
	printf("h,H - Ajuda \n");
	printf("******* Diversos ******* \n");
	printf("l,L - Alterna o calculo luz entre Z e eye (GL_LIGHT_MODEL_LOCAL_VIEWER)\n");
	printf("w,W - Wireframe \n");
	printf("f,F - Fill \n");
	printf("m - Ativa a musica ambiente \n");
	printf("******* Movimento ******* \n");
	printf("up  - Acelera \n");
	printf("down- Trava \n");
	printf("left- Vira rodas para a direita\n");
	printf("righ- Vira rodas para a esquerda\n");
	printf("******* Camara ******* \n");
	printf("F1 - Alterna camara da janela da Esquerda \n");
	printf("F2 - Alterna camara da janela da Direita \n");
	printf("PAGE_UP, PAGE_DOWN - Altera abertura da camara \n");
	printf("botao esquerdo + movimento na Janela da Direita altera o olhar \n");
	printf("ESC - Sair\n");
}

void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(1);
		break;
	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'l':
	case 'L':
		estado.localViewer = !estado.localViewer;
		break;
	case 'w':
	case 'W':
		glutSetWindow(estado.navigateSubwindow);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_TEXTURE_2D);
		glutSetWindow(estado.topSubwindow);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_TEXTURE_2D);
		break;
	case 's':
	case 'S':
		glutSetWindow(estado.navigateSubwindow);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_TEXTURE_2D);
		glutSetWindow(estado.topSubwindow);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_TEXTURE_2D);
		break;
	case 'm':
	case 'M':
		estado.tecla_m = AL_TRUE;
		break;
	case 'o':
	case 'O':
		estado.tecla_o = AL_TRUE;
		break;
	}

}

void SpecialKey(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN: estado.teclas.down = GL_TRUE;
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_TRUE;
		break;
	case GLUT_KEY_F1: estado.vista[JANELA_TOP] = !estado.vista[JANELA_TOP];
		break;
	case GLUT_KEY_F2: estado.vista[JANELA_NAVIGATE] = !estado.vista[JANELA_NAVIGATE];
		break;
	case GLUT_KEY_PAGE_UP:
		if (estado.camera.fov>20)
		{
			estado.camera.fov--;
			glutSetWindow(estado.navigateSubwindow);
			reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
			redisplayAll();
		}
		break;
	case GLUT_KEY_PAGE_DOWN:
		if (estado.camera.fov<130)
		{
			estado.camera.fov++;
			glutSetWindow(estado.navigateSubwindow);
			reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
			redisplayAll();
		}
		break;
	}

}

// Callback para interaccao via teclas especiais (largar na tecla)
void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN: estado.teclas.down = GL_FALSE;
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_FALSE;
		break;
	}
}

// Inicializa��es
void createDisplayLists(int janelaID, int janelaID2)
{
	modelo.house[janelaID] = glGenLists(2);
	glNewList(modelo.house[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	double altura = 0.5;
	const char * file = "Data/Planta1.txt";
	desenhaPlanta(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData1, altura);
	file = "Data/PlantaChao1.txt";
	desenhaChao(modelo.texID[janelaID][ID_TEXTURA_CHAO], file, blueprintData1, altura - 0.5);
	file = "Data/PlantaTeto1.txt";
	desenhaTeto(modelo.texID[janelaID][ID_TEXTURA_TETO], file, blueprintData1, altura + 0.5);
	file = "Data/Planta1.txt";
	desenhaPortasVerticais(modelo.texID[janelaID][ID_TEXTURA_PORTAS_V], file, blueprintData1, altura);
	desenhaPortasHorizontais(modelo.texID[janelaID][ID_TEXTURA_PORTAS_H], file, blueprintData1, altura);
	desenhaJanelas(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData1, altura);
	desenhaS(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData1, altura);
	//desenhaOffice(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData1, altura);
	desenhaObj(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData1, altura);
	glPopAttrib();
	glEndList();

	modelo.chao[janelaID] = modelo.house[janelaID] + 1;
	glNewList(modelo.chao[janelaID], GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	altura = 1.5;
	file = "Data/Planta2.txt";
	desenhaPlanta(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData2, altura);
	desenhaPortasVerticais(modelo.texID[janelaID][ID_TEXTURA_PORTAS_V], file, blueprintData2, altura);
	desenhaPortasHorizontais(modelo.texID[janelaID][ID_TEXTURA_PORTAS_H], file, blueprintData2, altura);
	desenhaJanelas(modelo.texID[janelaID][ID_TEXTURA_PAREDES], file, blueprintData2, altura);
	glPopAttrib();
	glEndList();
}

/// Texturas
// S� para windows (usa biblioteca glaux)
#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File = NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File = fopen(Filename, "r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif

void createTextures(GLuint texID[])
{
	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *) * 1);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#ifdef _WIN32

	char *parede = new char[texturas[1].length() + 1];
	strcpy(parede, texturas[1].c_str());

	if (TextureImage[0] = LoadBMP(parede))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_PAREDES]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
#else
	if (read_JPEG_file(NOME_TEXTURA_CUBOS, &image, &w, &h, &bpp))
	{
		// Create MipMapped Texture
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CUBOS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
#endif
	else
	{
		printf("Textura %s nao foi encontrada\n", parede);
		system("pause");
		exit(0);
	}
	delete[] parede;

	const char *chao = texturas[0].c_str();
	if (read_JPEG_file(chao, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else
	{
		printf("Textura %s nao foi encontrada\n", chao);
		system("pause");
		exit(0);
	}

	const char *teto = texturas[2].c_str();
	if (read_JPEG_file(teto, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_TETO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else
	{
		printf("Textura %s nao foi encontrada\n", teto);
		system("pause");
		exit(0);
	}

	const char * TexturaPortaVertical = texturas[3].c_str();
	if (read_JPEG_file(TexturaPortaVertical, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_PORTAS_V]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else
	{
		printf("Textura %s nao foi encontrada\n", TexturaPortaVertical);
		system("pause");
		exit(0);
	}

	const char * TexturaPortaHorizontal = texturas[4].c_str();
	if (read_JPEG_file(TexturaPortaHorizontal, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_PORTAS_H]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else
	{
		printf("Textura %s nao foi encontrada\n", TexturaPortaHorizontal);
		system("pause");
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void init_audio() {
	estado.buffer = alutCreateBufferFromFile("island_music.wav");
	alGenSources(1, &estado.source);
	alSourcei(estado.source, AL_BUFFER, estado.buffer);
	estado.tecla_m = AL_FALSE;
	estado.tecla_o = AL_FALSE;
}

void push() {
	glPushMatrix();
}
void pop() {
	glPopMatrix();
}

//Desenha
void init()
{
	GLfloat amb[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	estado.timer = 100;

	estado.camera.eye.x = 0;
	estado.camera.eye.y = OBJECTO_ALTURA * 2;
	estado.camera.eye.z = 0;
	estado.camera.dir_long = 0;
	estado.camera.dir_lat = 0;
	estado.camera.fov = 60;

	estado.localViewer = 1;
	estado.vista[JANELA_TOP] = 0;
	estado.vista[JANELA_NAVIGATE] = 0;

	modelo.objecto.pos.x = 0;
	modelo.objecto.pos.y = OBJECTO_ALTURA*1;
	modelo.objecto.pos.z = 0;
	modelo.objecto.dir = 0;
	modelo.objecto.vel = OBJECTO_VELOCIDADE;

	modelo.xMouse = modelo.yMouse = -1;
	modelo.andar = GL_FALSE;

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);  // por causa do Scale ao Homer

	if (glutGetWindow() == estado.mainWindow)
		glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	else
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	

	//OfficeRoomList = glGenLists(1);
	//glNewList(OfficeRoomList, GL_COMPILE);

	//glPushMatrix();
	//obj = glmReadOBJ("obj/Escritorio/Desk.obj");
	////glmLinearTexture(obj);
	//glmUnitize(obj);
	//glmFacetNormals(obj);
	//glmVertexNormals(obj, 45.0);
	//glmScale(obj, 0.3);
	//glTranslatef(-1, -0.25, -2);
	//glRotatef(30, 0, 1, 0);
	////glmDraw(obj, GLM_SMOOTH | GLM_COLOR);
	//glmDraw(obj, GLM_SMOOTH);
	////glmDraw(obj, 1);
	//glmDelete(obj);
	//glPopMatrix();

	//glPushMatrix();
	//obj = glmReadOBJ("obj/Escritorio/office_chair.obj");
	//glmLinearTexture(obj);
	//glmUnitize(obj);
	//glmFacetNormals(obj);
	//glmVertexNormals(obj, 45.0);
	//glmScale(obj, 0.25);
	//glTranslatef(-0.7, -0.25, -2);
	//glRotatef(45, 0, 1, 0);
	////glmDraw(obj, GLM_SMOOTH | GLM_COLOR);
	//glmDraw(obj, GLM_SMOOTH | GLM_TEXTURE);
	////glmDraw(obj, 1);
	//glmDelete(obj);
	//glPopMatrix();

	//glEndList();

	HorizontalWindowList = glGenLists(1);
	glNewList(HorizontalWindowList, GL_COMPILE);
	obj = glmReadOBJ("obj/window.obj");
	glmFacetNormals(obj);
	glmVertexNormals(obj, 45);
	glmLinearTexture(obj);
	glmScale(obj, 0.025);
	glmDraw(obj, GLM_SMOOTH | GLM_TEXTURE);
	glmDelete(obj);
	glEndList();

	VerticalWindowList = glGenLists(1);
	glNewList(VerticalWindowList, GL_COMPILE);
	obj = glmReadOBJ("obj/window.obj");
	glmFacetNormals(obj);
	glmVertexNormals(obj, 45);
	glmLinearTexture(obj);
	glmScale(obj, 0.025);
	glRotatef(90, 0, 1, 0);
	glmDraw(obj, GLM_SMOOTH | GLM_TEXTURE);
	glmDelete(obj);
	glEndList();

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
	//LoadMatrix();
	modelo.duracao = 0;
}

int main(int argc, char **argv)
{
	alutInit(&argc, argv);
	glutInit(&argc, argv);
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(800 + GAP * 3, 400 + GAP * 2);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	if ((estado.mainWindow = glutCreateWindow("house")) == GL_FALSE)
		exit(1);

	imprime_ajuda();
	LoadTexturas();

	// Registar callbacks do GLUT da janela principal
	init();
	
	glutReshapeFunc(reshapeMainWindow);
	glutDisplayFunc(displayMainWindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// criar a sub window topSubwindow
	estado.topSubwindow = glutCreateSubWindow(estado.mainWindow, GAP, GAP, 400, 400);
	init();
	init_audio();
	setLight();
	setMaterial();
	createTextures(modelo.texID[JANELA_TOP]);
	createDisplayLists(JANELA_TOP, JANELA_TOP2);

	mdlviewer_init("Data/putin.mdl", modelo.visitor[JANELA_TOP]);

	glutReshapeFunc(redisplayTopSubwindow);
	glutDisplayFunc(displayTopSubwindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);


	// criar a sub window navigateSubwindow
	estado.navigateSubwindow = glutCreateSubWindow(estado.mainWindow, 400 + GAP, GAP, 400, 800);
	init();
	setLight();
	setMaterial();

	createTextures(modelo.texID[JANELA_NAVIGATE]);
	createDisplayLists(JANELA_NAVIGATE, JANELA_NAVIGATE2);
	mdlviewer_init("Data/putin.mdl", modelo.visitor[JANELA_NAVIGATE]);

	glutReshapeFunc(reshapeNavigateSubwindow);
	glutDisplayFunc(displayNavigateSubwindow);
	glutMouseFunc(mouseNavigateSubwindow);

	glutTimerFunc(estado.timer, Timer, 0);
	glutKeyboardFunc(Key);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	
	glutMainLoop();
	return 0;
}
